#!/bin/env python3

import sys
from Crypto.Cipher import AES

# pycrypto python module is needed and can be installed by:
# pip install pycrypto
#
# Run with encrypted file as the first argument and decrypted filename as the second one, e.g.:
# ./question12.py alabaster_passwords.elfdb.wannacookie alabaster_passwords.elfdb

encrypted_file = open(sys.argv[1], 'rb')
encrypted = encrypted_file.read()
iv_length = int.from_bytes(encrypted[:4], 'little')
iv = encrypted[4:4+iv_length]
key = bytes([251, 207, 193, 33, 145, 93, 153, 204, 32, 163, 211, 213, 216, 79, 131, 8])
cipher = AES.new(key, AES.MODE_CBC, iv)
decrypted = cipher.decrypt(encrypted[4+iv_length:])
decrypted_file = open(sys.argv[2], 'wb')
decrypted_file.write(decrypted)
decrypted_file.close()
