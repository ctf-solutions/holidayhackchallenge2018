#!/bin/env python

import zlib
import base64
import sys

# Reads base64 encoded compressed data from stdin

print(zlib.decompress(base64.b64decode(sys.stdin.read()), -15))
